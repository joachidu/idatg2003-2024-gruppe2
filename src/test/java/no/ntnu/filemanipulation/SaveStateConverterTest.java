package no.ntnu.filemanipulation;

import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.JuliaTransform;
import no.ntnu.unitclasses.Complex;
import no.ntnu.unitclasses.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class SaveStateTest {

    private SaveState saveState;

    @BeforeEach
    void setUp() {
        int[][] canvas = new int[100][100];
        int height = 100;
        int width = 100;
        Vector2D minCoords = new Vector2D(0, 0);
        Vector2D maxCoords = new Vector2D(100, 100);
        List<AffineTransform2D> affineTransforms = new ArrayList<>();
        affineTransforms.add(new AffineTransform2D());
        List<JuliaTransform> juliaTransforms = new ArrayList<>();
        juliaTransforms.add(new JuliaTransform(new Complex(1,1), 1));
        int steps = 10;
        String transformationType = "Affine2D";
        saveState = new SaveState.SaveStateBuilder()
                .canvas(canvas)
                .height(height)
                .width(width)
                .minCoords(minCoords)
                .maxCoords(maxCoords)
                .affineTransforms(affineTransforms)
                .juliaTransforms(juliaTransforms)
                .steps(steps)
                .transformationType(transformationType)
                .build();
    }

    @Test
    void testConstructorAndGetters() {
        assertNotNull(saveState, "SaveState should not be null");
        assertEquals(100, saveState.getHeight(), "Height should match");
        assertEquals(100, saveState.getWidth(), "Width should match");
        assertEquals(0, saveState.getMinCoords().getX0(), "MinCoords X0 value should match");
        assertEquals(0, saveState.getMinCoords().getX1(), "MinCoords X1 value should match");
        assertEquals(100, saveState.getMaxCoords().getX0(), "MaxCoords X0 value should match");
        assertEquals(100, saveState.getMaxCoords().getX1(), "MaxCoords X1 value should match");
        assertEquals(1, saveState.getAffineTransforms().size(), "AffineTransforms list size should be 1");
        assertEquals(1, saveState.getJuliaTransforms().size(), "JuliaTransforms list size should be 1");
        assertEquals(10, saveState.getSteps(), "Steps should match");
        assertEquals("Affine2D", saveState.getTransformationType(), "TransformationType should match");
    }

    @Test
    void testSetters() {
        int[][] newCanvas = new int[200][200];
        Vector2D newMinCoords = new Vector2D(-10, -10);
        Vector2D newMaxCoords = new Vector2D(200, 200);
        List<AffineTransform2D> newAffineTransforms = new ArrayList<>();
        newAffineTransforms.add(new AffineTransform2D());
        List<JuliaTransform> newJuliaTransforms = new ArrayList<>();
        newJuliaTransforms.add(new JuliaTransform(new Complex(1,1), 1));
        int newSteps = 20;
        String newTransformationType = "Affine2D";

        saveState.setCanvas(newCanvas);
        saveState.setHeight(200);
        saveState.setWidth(200);
        saveState.setMinCoords(newMinCoords);
        saveState.setMaxCoords(newMaxCoords);
        saveState.setAffineTransforms(newAffineTransforms);
        saveState.setJuliaTransforms(newJuliaTransforms);
        saveState.setSteps(newSteps);
        saveState.setTransformType(newTransformationType);

        assertEquals(200, saveState.getHeight(), "Height should match after setting");
        assertEquals(200, saveState.getWidth(), "Width should match after setting");
        assertEquals(-10, saveState.getMinCoords().getX0(), "MinCoords X0 value should match after setting");
        assertEquals(-10, saveState.getMinCoords().getX1(), "MinCoords X1 value should match after setting");
        assertEquals(200, saveState.getMaxCoords().getX0(), "MaxCoords X0 value should match after setting");
        assertEquals(200, saveState.getMaxCoords().getX1(), "MaxCoords X1 value should match after setting");
        assertEquals(1, saveState.getAffineTransforms().size(), "AffineTransforms list size should be 1 after setting");
        assertEquals(1, saveState.getJuliaTransforms().size(), "JuliaTransforms list size should be 1 after setting");
        assertEquals(20, saveState.getSteps(), "Steps should match after setting");
        assertEquals("Affine2D", saveState.getTransformationType(), "TransformationType should match after setting");
    }

}
