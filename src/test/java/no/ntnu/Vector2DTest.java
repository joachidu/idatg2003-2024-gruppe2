package no.ntnu;



import static org.junit.jupiter.api.Assertions.*;

import no.ntnu.unitclasses.Vector2D;
/*
 * The Vector2DTest class is a test class for the Vector2D class.
 * It tests the methods in the Vector2D class, both positive and negative.
 */
class Vector2DTest {
    Vector2D vector;
    /*
     * The setUp method initializes the vector object before each test.
     */
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        vector = new Vector2D(1, 2);
    }
    /*
     * The tearDown method sets the vector object to null after each test.
     */
    @org.junit.jupiter.api.AfterEach
    void tearDown() {

    }
    /*
     * The PositiveTestGetX0 method tests the getX0 method in the Vector2D class.
     * It tests if the method returns the correct value.
     */
    @org.junit.jupiter.api.Test
    void positiveTestGetX0() {
        assertEquals(1, vector.getX0(), "Positive test for getX0 is working");
    }
    /**
     * The PositiveTestGetX1 method tests the getX1 method in the Vector2D class.
     * It tests if the method returns the correct value.
     */
    @org.junit.jupiter.api.Test
    void getX1() {
        assertEquals(2, vector.getX1(), "Positive test for getX1 is working");
    }
    /**
     * Positive test for add method in the Vector2D class.
     */
    @org.junit.jupiter.api.Test
    void positiveAddTest() {
        Vector2D vector2 = new Vector2D(2, 3);
        Vector2D expected = new Vector2D(3, 5);
        assertEquals(expected, vector.add(vector2), "Positive test for add is working");
    }

    /*
     * Positive test for subtract method in the Vector2D class.
     */
    @org.junit.jupiter.api.Test
    void subtract() {
        Vector2D vector2 = new Vector2D(2, 3);
        Vector2D expected = new Vector2D(-1, -1);;
        assertEquals(expected, vector.subtract(vector2), "Positive test for subtract is working");
    }

    /*
     * Negative test for add method in the Vector2D class.
     * It tests if the method throws an IllegalArgumentException when the given vector is null.
     */
    @org.junit.jupiter.api.Test
    void NegativeAddTest() {
        Vector2D vector2 = null;
        assertThrows(IllegalArgumentException.class, () -> vector.add(vector2), "Negative test for add is working");
    }

    /*
     * Negative test for subtract method in the Vector2D class.
     * It tests if the method throws an IllegalArgumentException when the given vector is null.
     */
    @org.junit.jupiter.api.Test
    void NegativeSubtractTest() {
        Vector2D vector2 = null;
        assertThrows(IllegalArgumentException.class, () -> vector.subtract(vector2), "Negative test for subtract is working");
    }
}