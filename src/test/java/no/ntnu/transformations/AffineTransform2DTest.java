package no.ntnu.transformations;

import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.unitclasses.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AffineTransform2DTest   {

    private AffineTransform2D affineTransform2D;
    public static final String SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION = "Should throw IllegalArgumentException";
    @BeforeEach
    void setUp() {
        Matrix2D matrix = new Matrix2D(1, 2, 3, 4);
        Vector2D vector = new Vector2D(1, 2);
        affineTransform2D = new AffineTransform2D(matrix, vector);
    }

    @Test
    void testTransform() {
        Vector2D inputVector = new Vector2D(2, 3);
        Vector2D result = affineTransform2D.transform(inputVector);
        Vector2D expected = new Vector2D(9, 20);

        assertNotNull(result, "Result should not be null");
        assertEquals(expected.getX0(), result.getX0(), "X0 value should match");
        assertEquals(expected.getX1(), result.getX1(), "X1 value should match");
    }

    @Test
    void testTransformNegative() {
        assertThrows(IllegalArgumentException.class, () -> affineTransform2D.transform(null), SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }

    @Test
    void getMatrix() {
        assertEquals(1, affineTransform2D.getMatrix().getMatrix()[0][0], "Expected value is 1, but got " + affineTransform2D.getMatrix().getMatrix()[0][0]);
        assertEquals(2, affineTransform2D.getMatrix().getMatrix()[0][1], "Expected value is 2, but got " + affineTransform2D.getMatrix().getMatrix()[0][1]);
        assertEquals(3, affineTransform2D.getMatrix().getMatrix()[1][0], "Expected value is 3, but got " + affineTransform2D.getMatrix().getMatrix()[1][0]);
        assertEquals(4, affineTransform2D.getMatrix().getMatrix()[1][1], "Expected value is 4, but got " + affineTransform2D.getMatrix().getMatrix()[1][1]);
    }

    @Test
    void getVector() {
        assertEquals(1, affineTransform2D.getVector().getX0(), "Expected value is 1, but got " + affineTransform2D.getVector().getX0());
        assertEquals(2, affineTransform2D.getVector().getX1(), "Expected value is 2, but got " + affineTransform2D.getVector().getX1());
    }

    @Test
    void setMatrixTest() {
        Matrix2D matrix = new Matrix2D(8, 9, 11, 12);
        affineTransform2D.setMatrix(matrix);
        assertEquals(8, affineTransform2D.getMatrix().getMatrix()[0][0], "Expected value is 8, but got " + affineTransform2D.getMatrix().getMatrix()[0][0]);
        assertEquals(9, affineTransform2D.getMatrix().getMatrix()[0][1], "Expected value is 9, but got " + affineTransform2D.getMatrix().getMatrix()[0][1]);
        assertEquals(11, affineTransform2D.getMatrix().getMatrix()[1][0], "Expected value is 11, but got " + affineTransform2D.getMatrix().getMatrix()[1][0]);
        assertEquals(12, affineTransform2D.getMatrix().getMatrix()[1][1], "Expected value is 12, but got " + affineTransform2D.getMatrix().getMatrix()[1][1]);
    }

    @Test
    void negativeTestSetMatrix() {
        assertThrows(IllegalArgumentException.class, () -> affineTransform2D.setMatrix(null), SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }

    @Test
    void setVector() {
        Vector2D vector = new Vector2D(8, 9);
        affineTransform2D.setVector(vector);
        assertEquals(8, affineTransform2D.getVector().getX0(), "Expected value is 8, but got " + affineTransform2D.getVector().getX0());
        assertEquals(9, affineTransform2D.getVector().getX1(), "Expected value is 9, but got " + affineTransform2D.getVector().getX1());
    }

    @Test
    void negativeTestSetVector() {
        assertThrows(IllegalArgumentException.class, () -> affineTransform2D.setVector(null), SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }
}