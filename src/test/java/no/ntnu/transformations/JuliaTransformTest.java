package no.ntnu.transformations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.ntnu.unitclasses.Complex;
import no.ntnu.unitclasses.Vector2D;

import static org.junit.jupiter.api.Assertions.*;

class JuliaTransformTest {

    private JuliaTransform juliaTransform;

    @BeforeEach
    void setUp() {
        Complex initialPoint = new Complex(1, 1);
        juliaTransform = new JuliaTransform(initialPoint, 1);
    }

    @Test
    void setPoint() {
        Complex newPoint = new Complex(2, 3);
        juliaTransform.setPoint(newPoint);
        assertEquals(newPoint, juliaTransform.getPoint(), "Point should be (2, 3)");
        assertThrows(IllegalArgumentException.class, () -> juliaTransform.setPoint((Complex) null),
                AffineTransform2DTest.SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }

    @Test
    void testSetPoint() {
        Vector2D newPoint = new Vector2D(2, 3);
        juliaTransform.setPoint(newPoint);
        Complex expectedComplexPoint = new Complex(2, 3);
        assertEquals(expectedComplexPoint, juliaTransform.getPoint(), "Point should be (2, 3)");
        assertThrows(IllegalArgumentException.class, () -> juliaTransform.setPoint((Vector2D) null),
                        AffineTransform2DTest.SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }

    @Test
    void setSign() {
        juliaTransform.setSign(-1);
        assertEquals(-1, juliaTransform.getSign(), "Sign should be -1");
        assertThrows(IllegalArgumentException.class, () -> juliaTransform.setSign(0),
                        AffineTransform2DTest.SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
        assertThrows(IllegalArgumentException.class, () -> juliaTransform.setSign(2),
                        AffineTransform2DTest.SHOULD_THROW_ILLEGAL_ARGUMENT_EXCEPTION);
    }


    @Test
    void testTransformComplexPointPositiveValues() {
        Complex point = new Complex(1, 1);
        JuliaTransform jt = new JuliaTransform(point, 1);
        Vector2D z = new Vector2D(1, 1);
        Vector2D result = jt.transform(z);
        assertEquals(0, result.getX0(), "X0 should be 0, but got " + result.getX0());
        assertEquals(0, result.getX1(), "X1 should be 0, but got " + result.getX1());
    }

    @Test
    void testTransformComplexPointNegativeValues() {
        Complex point = new Complex(-1, -1);
        JuliaTransform jt = new JuliaTransform(point, 1);
        Vector2D z = new Vector2D(-1, -1);
        Vector2D result = jt.transform(z);
        assertEquals(0, result.getX0(), "X0 should be 0, but got " + result.getX0());
        assertEquals(0, result.getX1(), "X1 should be 0, but got " + result.getX1());
    }

    @Test
    void testTransformComplexPointMixedValues() {
        Complex point = new Complex(-1, 1);
        JuliaTransform jt = new JuliaTransform(point, 1);
        Vector2D z = new Vector2D(1, -1);
        Vector2D result = jt.transform(z);
        assertNotNull(result, "Result should not be null");

        point = new Complex(1, -1);
        jt = new JuliaTransform(point, 1);
        z = new Vector2D(-1, 1);
        result = jt.transform(z);
        assertNotNull(result, "Result should not be null");
    }
}
