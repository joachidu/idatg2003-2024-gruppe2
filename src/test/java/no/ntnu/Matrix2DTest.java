package no.ntnu;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.ntnu.unitclasses.Matrix2D;

public class Matrix2DTest {
    Matrix2D matrix = new Matrix2D();
    @BeforeEach
    void setUp() {
        matrix = new Matrix2D();
    }

    @Test
    void testGetMatrix() {
        double[][] expected = new double[2][2];
        double[][] actual = matrix.getMatrix();
        assertArrayEquals(expected, actual);
    }

    @Test
    void testSetMatrix4Variables() {
        double[][] expected = new double[][]{{1, 2}, {3, 4}};
        matrix.setMatrix(1, 2, 3, 4);
        double[][] actual = matrix.getMatrix();
        assertArrayEquals(expected, actual);
    }

    @Test
    void testSetMatrixMatrixAsParameter() {
        double[][] expected = new double[][]{{1, 2}, {3, 4}};
        double[][] input = new double[][]{{1, 2}, {3, 4}};
        matrix.setMatrix(input);
        double[][] actual = matrix.getMatrix();
        assertArrayEquals(expected, actual);
    }
}
