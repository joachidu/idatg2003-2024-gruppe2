package no.ntnu.chaosgame;

import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.transformations.Transform2D;
import no.ntnu.unitclasses.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class ChaosGameDescriptionTest {

    private ChaosGameDescription chaosGameDescription;

    @BeforeEach
    void setUp() {
        Vector2D minCoords = new Vector2D(0, 0);
        Vector2D maxCoords = new Vector2D(100, 100);
        List<Transform2D> transforms = new ArrayList<>();
        Matrix2D matrix = new Matrix2D(1, 0, 0, 1);
        Vector2D vector = new Vector2D(0, 0);
        transforms.add(new AffineTransform2D(matrix, vector));
        Matrix2D matrix2 = new Matrix2D(3, 0, 0, 1);
        Vector2D vector2 = new Vector2D(1, 0);
        transforms.add(new AffineTransform2D(matrix2, vector2));
        String transformType = "Affine2D";
        chaosGameDescription = new ChaosGameDescription(minCoords, maxCoords, transforms, transformType);
    }

    @Test
    void testConstructorAndGetters() {
        assertNotNull(chaosGameDescription, "ChaosGameDescription should not be null");
        assertEquals(0, chaosGameDescription.getMinCoords().getX0(), "MinCoords X0 value should match");
        assertEquals(0, chaosGameDescription.getMinCoords().getX1(), "MinCoords X1 value should match");
        assertEquals(100, chaosGameDescription.getMaxCoords().getX0(), "MaxCoords X0 value should match");
        assertEquals(100, chaosGameDescription.getMaxCoords().getX1(), "MaxCoords X1 value should match");
        assertEquals(2, chaosGameDescription.getTransforms().size(), "Transforms list size should be 2");
        assertEquals("Affine2D", chaosGameDescription.getTransformType(), "TransformType should match");
    }

    @Test
    void testSetTransformType() {
        chaosGameDescription.setTransformType("Type2");
        assertEquals("Type2", chaosGameDescription.getTransformType(), "TransformType should match after setting");
    }

    @Test
    void testSetMinCoords() {
        Vector2D newMinCoords = new Vector2D(-10, -10);
        chaosGameDescription.setMinCoords(newMinCoords);
        assertEquals(-10, chaosGameDescription.getMinCoords().getX0(), "MinCoords X0 value should match after setting");
        assertEquals(-10, chaosGameDescription.getMinCoords().getX1(), "MinCoords X1 value should match after setting");
    }

    @Test
    void testSetMaxCoords() {
        Vector2D newMaxCoords = new Vector2D(200, 200);
        chaosGameDescription.setMaxCoords(newMaxCoords);
        assertEquals(200, chaosGameDescription.getMaxCoords().getX0(), "MaxCoords X0 value should match after setting");
        assertEquals(200, chaosGameDescription.getMaxCoords().getX1(), "MaxCoords X1 value should match after setting");
    }

    @Test
    void testSetTransforms() {
        List<Transform2D> newTransforms = new ArrayList<>();
        Matrix2D matrix = new Matrix2D(3, 0, 0, 3);
        Vector2D vector = new Vector2D(0, 0);
        newTransforms.add(new AffineTransform2D(matrix, vector));
        chaosGameDescription.setTransforms(newTransforms);
        assertEquals(1, chaosGameDescription.getTransforms().size(), "Transforms list size should be 1 after setting");
    }

    @Test
    void testNullMinCoords() {
        assertThrows(IllegalArgumentException.class, () -> chaosGameDescription.setMinCoords(null), "Setting null MinCoords should throw IllegalArgumentException");
    }

    @Test
    void testNullMaxCoords() {
        assertThrows(IllegalArgumentException.class, () -> chaosGameDescription.setMaxCoords(null), "Setting null MaxCoords should throw IllegalArgumentException");
    }

    @Test
    void testNullTransforms() {
        assertThrows(IllegalArgumentException.class, () -> chaosGameDescription.setTransforms(null), "Setting null Transforms should throw IllegalArgumentException");
    }

    @Test
    void testInvalidMinCoords() {
        Vector2D invalidMinCoords = new Vector2D(101, 101);
        assertThrows(IllegalArgumentException.class, () -> chaosGameDescription.setMinCoords(invalidMinCoords), "Setting invalid MinCoords should throw IllegalArgumentException");
    }

    @Test
    void testInvalidMaxCoords() {
        Vector2D invalidMaxCoords = new Vector2D(-10, -10);
        assertThrows(IllegalArgumentException.class, () -> chaosGameDescription.setMaxCoords(invalidMaxCoords), "Setting invalid MaxCoords should throw IllegalArgumentException");
    }
}
