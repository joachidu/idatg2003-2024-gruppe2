package no.ntnu.chaosgame;


import no.ntnu.unitclasses.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameCanvasTest {

    ChaosGameCanvas chaosGameCanvas;
    @BeforeEach
    void setUp() {
        chaosGameCanvas = new ChaosGameCanvas(600, 600, new Vector2D(0, 0), new Vector2D(1, 1));
    }

    @Test
    void getHeight() {
        assertEquals(600, chaosGameCanvas.getHeight(), "Height should be 600, but was " + chaosGameCanvas.getHeight());
    }

    @Test
    void getWidth() {
        assertEquals(600, chaosGameCanvas.getWidth(), "Width should be 600, but was " + chaosGameCanvas.getWidth());
    }

    @Test
    void clearCanvas() {
        chaosGameCanvas.getCanvas()[0][0] = 1;
        chaosGameCanvas.clearCanvas();
        assertEquals(0, chaosGameCanvas.getCanvas()[0][0], "Canvas should be cleared, but was not");
    }

    @Test
    void putPixel(){
        chaosGameCanvas.putPixel(new Vector2D(0, 0));
        assertEquals(1, chaosGameCanvas.getCanvas()[599][0], "Canvas should have a pixel at 0, 0, but did not");
    }
    @Test
    void getMaxCoords() {
        Vector2D maxCoords = new Vector2D(1,1);
        assertEquals(maxCoords, chaosGameCanvas.getMaxCoords(), "MaxCoords should be 1,1, but was " + chaosGameCanvas.getMaxCoords());
    }

    @Test
    void getMinCoords() {
        Vector2D minCoords = new Vector2D(0,0);
        assertEquals(minCoords, chaosGameCanvas.getMinCoords(), "MinCoords should be 0,0, but was " + chaosGameCanvas.getMinCoords());
    }

    @Test
    void getCanvas() {
        assertEquals(600, chaosGameCanvas.getCanvas().length, "Canvas should be 600, but was " + chaosGameCanvas.getCanvas().length);
    }

    @Test
    void setCanvas() {
        int[][] canvas = new int[600][600];
        chaosGameCanvas.setCanvas(canvas);
        assertEquals(canvas, chaosGameCanvas.getCanvas(), "Canvas should be set to the new canvas, but was not");
    }
}