package no.ntnu;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.ntnu.unitclasses.Complex;

class ComplexTest {

    Complex complex;
    @BeforeEach
    void setUp() {
        complex = new Complex(1, 2);
    }

    @Test
    void testComplex() {
        assertEquals(1, complex.getX0(), "Positive test for getX0 is working");
        assertEquals(2, complex.getX1(), "Positive test for getX1 is working");
    }

    @Test
    void testComplexSqrt() {
        Complex sqrt = complex.sqrt();
        assertEquals(1.272, sqrt.getX0(), 0.001, "Positive test for getX0 is working");
        assertEquals(0.786, sqrt.getX1(), 0.001, "Positive test for getX1 is working");
    }
}