package no.ntnu.trackers;
import java.util.Map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DuplicateTracker<T> {
    private Map<T, Integer> duplicates;
    private List<T> allPoints;

    public DuplicateTracker() {
        this.duplicates = new HashMap<>();
        this.allPoints = new ArrayList<>();
    }

    public List<T> getAllPoints() {
        return allPoints;
    }

    public void addObject(T object) {
        allPoints.add(object);
        Integer occurances = duplicates.get(object);
        if (occurances == null) {
            occurances = 0;
        }
        duplicates.put(object, occurances + 1);
    }

    public int getOccurances(T object) {
        Integer occurances = duplicates.get(object);
        if (occurances == null) {
            return 0;
        }
        return occurances;
    }
}
