package no.ntnu.trackers;

import java.util.HashSet;

/**
 * The {@code ControllerTracker} class is a class that represents a tracker for loaded controllers.
 *
 * @since 0.0.1
 * @version 0.0.1
 * @author Joachim Duong
 */
public class ControllerTracker {
    
    private static HashSet<Class<?>> loadedControllers = new HashSet<>();

    /**
     * Private constructor to prevent instantiation.
     */
    private ControllerTracker() {
    }

    public static boolean isControllerLoaded(Class<?> controller) {
        return loadedControllers.contains(controller);
    }

    public static void setControllerLoaded(Class<?> controller) {
        loadedControllers.add(controller);
    }
}
