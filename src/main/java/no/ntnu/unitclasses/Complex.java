package no.ntnu.unitclasses;

/**
 * The {@code Complex} is a class that inherits from Vector2D and
 * represents a vector that have at least one of its values as a complex number.
 * Like Vector2D, it is a vector with 2 dimensions, but its methods differs.
 * All {@code Comples}s have a x0 and x1 value.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *     Vector2D vector = new Vector2D(PARAMETERS);
 * </pre></blockquote>
 *
 * <p>The class {@code Vector2D} includes methods adding and subtracting other vectors
 * with it.
 *
 * @author Joachim Duong
 * @version 0.0.1
 * @since 0.0.1
 */
public class Complex extends Vector2D{

    /**
     * Constructs a {@code Vector2D} with the specified x0 and x1 values.
     * @param realPart the first value of the vector
     * @param imaginaryPart the second value of the vector
     * @throws IllegalArgumentException if the specified vector contains NaN values
     * @since 0.0.1
     */
    public Complex(double realPart, double imaginaryPart) {
        super(realPart, imaginaryPart);
        if (Double.isNaN(realPart) || Double.isNaN(imaginaryPart)) {
            throw new IllegalArgumentException("Vector cannot contain NaN values");
        }
    }

    /**
     * Constructs a {@code Vector2D} with the specified vector.
     * @param vector the vector to be created. Cannot be null
     * @throws IllegalArgumentException if the specified vector is null
     * @since 0.0.1
     */
    public Complex(Vector2D vector) {
        super(vector.getX0(), vector.getX1());
    }

    /**
     * Method for getting the square root of the complex number.
     * It separates the real and imaginary part of the result and returns them as
     * a new Complex object.
     * @return a new Complex object with the square root of the complex number
     * @since 0.0.1
     */
    public Complex sqrt(){
        double realPart = Math.sqrt((getX0() + Math.sqrt(getX0() * getX0() + getX1() * getX1())) / 2);
        double imaginaryPart = Math.sqrt((-getX0() + Math.sqrt(getX0() * getX0() + getX1() * getX1())) / 2);
        if(getX1() < 0){
            imaginaryPart = -imaginaryPart;
        }

        return new Complex(realPart, imaginaryPart);
    }
}
