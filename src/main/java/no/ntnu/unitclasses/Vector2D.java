package no.ntnu.unitclasses;

/**
 * The {@code Vector2D} class represents a vector with 2 dimensions.
 * All {@code Vector2D}s have a x0 and x1 value.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *     Vector2D vector = new Vector2D(PARAMETERS);
 * </pre></blockquote>
 *
 * <p>The class {@code Vector2D} includes methods adding and subtracting other vectors
 * with it.
 *
 * @author Joachim Duong
 * @version 0.0.1
 * @since 0.0.1
 */
public class Vector2D {

    private double x0;
    private double x1;
    private static final String VECTOR_CANNOT_BE_NULL = "Vector cannot be null";
    private static final String VECTOR_CANNOT_CONTAIN_NAN_VALUES = "Vector cannot contain NaN values";

    /**
     * Constructs a {@code Vector2D} with the specified x0 and x1 values.
     * @param x0 the first value of the vector
     * @param x1 the second value of the vector
     * @throws IllegalArgumentException if the specified vector contains NaN values¨
     * @since 0.0.1
     */
    public Vector2D(double x0, double x1) {
        if (Double.isNaN(x0) || Double.isNaN(x1)) {
            throw new IllegalArgumentException(VECTOR_CANNOT_CONTAIN_NAN_VALUES);
        }
        this.x0 = x0;
        this.x1 = x1;
    }

    /**
     * Constructs a {@code Vector2D} with all elements set to 0.
     * @since 0.0.1
     */
    public Vector2D() {
        this(0, 0);
    }

    /**
     * @return the first value of the vector at index 0
     * @since 0.0.1
     */
    public double getX0() {
        return x0;
    }

    /**
     * @return the second value of the vector at index 1
     * @since 0.0.1
     */
    public double getX1() {
        return x1;
    }

    /**
     * Adds the specified vector to this vector.
     * @param other the vector to add to this vector
     * @return a new vector with the sum of this vector and the specified vector
     * @throws IllegalArgumentException if the specified vector is null
     * @since 0.0.1
     */
    public Vector2D add(Vector2D other) {
        if (other == null) {
            throw new IllegalArgumentException(VECTOR_CANNOT_BE_NULL);
        }
        return new Vector2D(x0 + other.getX0(), x1 + other.getX1());
    }

    /**
     * Subtracts the specified vector from this vector.
     * @param other the vector to subtract from this vector
     * @return a new vector with the difference of this vector and the specified vector
     * @throws IllegalArgumentException if the specified vector is null
     * @since 0.0.1
     */
    public Vector2D subtract(Vector2D other) {
        if (other == null) {
            throw new IllegalArgumentException(VECTOR_CANNOT_BE_NULL);
        }
        return new Vector2D(x0 - other.getX0(), x1 - other.getX1());
    }

    /**
     * @param x0 the first value of the vector
     * @throws IllegalArgumentException if the specified vector contains NaN values
     * @since 0.0.1
     */
    public void setX0(double x0) {
        if (Double.isNaN(x0)) {
            throw new IllegalArgumentException(VECTOR_CANNOT_CONTAIN_NAN_VALUES);
        }
        this.x0 = x0;
    }

    /**
     * @param x1 the second value of the vector
     * @throws IllegalArgumentException if the specified vector contains NaN values
     * @since 0.0.1
     */
    public void setX1(double x1) {
        if (Double.isNaN(x1)) {
            throw new IllegalArgumentException(VECTOR_CANNOT_CONTAIN_NAN_VALUES);
        }
        this.x1 = x1;
    }

    /**
     * Checks if any of the elements in the vector is NaN.
     * @return true if the vector contains any NaN values, false otherwise.
     */
    public boolean containsNaNValues() {
        return Double.isNaN(x0) || Double.isNaN(x1);
    }

    @Override
    public int hashCode() {
        String x0Prefix = "";
        if (x0 < 10) {
            x0Prefix += "00";
        } else if (x0 < 100) {
            x0Prefix += "0";
        }
        String x1Prefix = "";
        if (x1 < 10) {
            x1Prefix += "00";
        } else if (x1 < 100) {
            x1Prefix += "0";
        }
        return Integer.valueOf("1" + x0Prefix + (int)x0 + x1Prefix + (int)x1);
    }

    /**
     *
     * @param obj the object to compare with
     * @return true if the specified object is equal to this vector, false otherwise
     * @since 0.0.1
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Vector2D)) {
            return false;
        }
        Vector2D other = (Vector2D) obj;
        return x0 == other.x0 && x1 == other.x1;
    }
}
