package no.ntnu.unitclasses;

/**
 * The {@code Matrix2D} class represents a 2x2 matrix.
 * All {@code Matrix2D}s have a 2x2 matrix.
 * 
 * <p>Example:
 * 
 * <blockquote><pre>
 *    Matrix2D matrix = new Matrix2D(PARAMETERS);
 * </pre></blockquote>
 * 
 * <p>The class {@code Matrix2D} includes methods for adding and subtracting other matrices
 * with it.
 * 
 * @author Rolf Normann Flatøy
 * @version 0.0.1
 * @since 0.0.1
 */
public class Matrix2D {

    /*
     * A 2x2 array with the matrix's values in doubles.
     */
    private double[][] matrix;

    /**
     * Creates a new 2x2 matrix with all elements set to 0.
     * @since 0.0.1
     */
    public Matrix2D() {
        initializeMatrix();

    }

    /**
     * Creates a new 2x2 matrix with the given matrix.
     * @param matrix the matrix to be created. Cannot be null
     * @since 0.0.1
     */
    public Matrix2D(double[][] matrix) {
        initializeMatrix();
        setMatrix(matrix);
    }

    /**
     * Creates a new 2x2 matrix with the given elements.
     * @param a00 the element in the first row and first column
     * @param a01 the element in the first row and second column
     * @param a10 the element in the second row and first column
     * @param a11 the element in the second row and second column
     * @since 0.0.1
     */
    public Matrix2D(double a00, double a01, double a10, double a11) {
        initializeMatrix();
        setMatrix(a00, a01, a10, a11);
    }

    /**
     * @return the matrix of this matrix
     * @since 0.0.1
     */
    public double[][] getMatrix() {
        return matrix;
    }

    /**
     * Sets the matrix of this matrix to the specified matrix.
     * @param a00 the element in the first row and first column
     * @param a01 the element in the first row and second column
     * @param a10 the element in the second row and first column
     * @param a11 the element in the second row and second column
     * @throws IllegalArgumentException if any of the doubles are NaN
     * @since 0.0.1
     */
    public void setMatrix(double a00, double a01, double a10, double a11) {
        if (Double.isNaN(a00) || Double.isNaN(a01) || Double.isNaN(a10) || Double.isNaN(a11)) {
            throw new IllegalArgumentException("Matrix elements cannot be NaN");
        }
        if (matrix == null) {
            throw new IllegalArgumentException("Matrix cannot be null");            
        }
            
        this.matrix = new double[][]{{a00, a01}, {a10, a11}};
    }

    /**
     * Sets the matrix of this matrix to the specified matrix.
     * @param matrix the matrix to set. Cannot be null
     * @throws IllegalArgumentException if the matrix is null
     *  @since 0.0.1
     */
    public void setMatrix(double[][] matrix) {
        if (matrix == null)
            throw new IllegalArgumentException("Matrix cannot be null");
        this.matrix = matrix;
    }

    /**
     * Multiplies the specified vector with this matrix.
     * @param vector the matrix to multiply to this matrix
     * @return a new matrix with the product of this matrix and the specified vector
     * @throws IllegalArgumentException if the vector given is null
     * @since 0.0.1
     */
    public Vector2D mulitply(Vector2D vector){
        if (vector == null)
            throw new IllegalArgumentException("Vector cannot be null");
        double x0 = matrix[0][0] * vector.getX0() + matrix[0][1] * vector.getX1();
        double x1 = matrix[1][0] * vector.getX0() + matrix[1][1] * vector.getX1();
        return new Vector2D(x0, x1);
    }

    /**
     * Checks if any of the elements in the matrix is NaN.
     * @return true if the matrix contains any NaN values, false otherwise.
     * @since 0.0.1
     */
    public boolean containsNaNValues() {
        for (double[] d : matrix) {
            for (double e : d) {
                if (Double.isNaN(e)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Initializes the matrix to a 2x2 array with 0 values
     * @since 0.0.1
     */
    public void initializeMatrix(){
        matrix = new double[2][2];
    }
}