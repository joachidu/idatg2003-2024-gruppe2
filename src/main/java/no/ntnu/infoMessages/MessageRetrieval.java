package no.ntnu.infoMessages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import java.util.Locale;

/**
 * The {@code MessageRetrieval} class is a utility class that retrieves messages from a properties file.
 * The class is used to retrieve messages in different languages.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *     MessageRetrieval.getString("key"); // Returns the message corresponding to the key
 * </pre></blockquote></p>
 *
 * @version 0.0.1
 * @since 0.0.1
 * @author Rolf Normann Flatøy
 */
public class MessageRetrieval {
    private static ResourceBundle bundle = ResourceBundle.getBundle("languages.Messages", Locale.forLanguageTag("en-US"));
    private static Logger logger = LogManager.getLogger(MessageRetrieval.class);

    // Private constructor to prevent instantiation.
    private MessageRetrieval() {
    }

    public static String getString(String key) {
        try {
            return bundle.getString(key);
        } catch (NullPointerException nullPointerException) {
            logger.error("Key was null.", nullPointerException);
        } catch (MissingResourceException missingResourceException) {
            logger.error("The corresponding string was not found", missingResourceException);
        } catch (ClassCastException classCastException) {
            logger.error("Returned value was not a string.", classCastException);
        }
        return "Could not find or load status message.";
    }
}
