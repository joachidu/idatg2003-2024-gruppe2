package no.ntnu;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.util.Locale;

/**
 * The entrypoint for the JavaFX application. 
 * 
 * @since 0.0.1
 * @version 0.0.1
 * @author Rolf Normann Flatøy
 */
public class FxApplication extends Application {
    private static Logger logger = LogManager.getLogger(FxApplication.class);

    @Override
    public void start(Stage stage) throws IOException {
        Locale.setDefault(Locale.forLanguageTag("en-US"));

        Parent root = loadFXML("HomeView");
        Scene scene = new Scene(root);

        Screen screen = Screen.getPrimary();

        double screenWidth = screen.getBounds().getWidth();
        double screenHeight = screen.getBounds().getHeight();

        stage.setHeight(screenHeight);
        stage.setWidth(screenWidth);
        stage.setScene(scene);
        stage.setTitle("Fractorial");
        stage.show();

    }

    /**
     * Starts the fx application, this should always be run first. 
     * @param args the start arguments passed in when launching the java application
     */
    public void startFX(String[] args) {
        launch(args);
    }

    /**
     * Loads the fxml file and returns the parent node. 
     * @param fxmlFileName the name of the fxml file to load
     * @return the parent node of the fxml file
     * @throws IOException if the fxml file could not be loaded
     */
    public static Parent loadFXML(String fxmlFileName) {
        try {
            return FXMLLoader.load(FxApplication.class.getResource("/FxmlFiles/" + fxmlFileName + ".fxml"));
        } catch (IOException ioException) {
            logger.error("Failed to load FXML file.", ioException);
            Alert criticalError = new Alert(AlertType.ERROR, "Failed to read file '" + fxmlFileName + ".fxml'. This is a " + 
            "critical error, the application will now exit.");
            criticalError.showAndWait();
            System.exit(1);
            return null;
        }
    }
}
