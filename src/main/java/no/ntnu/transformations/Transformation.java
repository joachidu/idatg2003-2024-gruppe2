package no.ntnu.transformations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.unitclasses.Complex;
import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.unitclasses.Vector2D;
import java.util.List;
import java.util.ArrayList;

/**
 * The {@code Transformation} class represents a transformation and contains all the data
 * needed to represent a transformation. It is meant to store the data read from a file.
 * The class includes a transformation name, a lower corner, an upper corner and a list of transformations.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *     Transformation transformation = new Transformation("Barnsley Fern", new double[] {0, 0}, new double[] {1, 1}, new ArrayList<double[]>());
 *     transformation.getTransformationName(); // Returns "Barnsley Fern"
 *     transformation.getLowerCorner(); // Returns {0, 0}
 *     transformation.getUpperCorner(); // Returns {1, 1}
 *     transformation.getTransformations(); // Returns an empty list
 *     transformation.setTransformationName("Sierpinski"); // Sets the transformation name to "Sierpinski"
 *     transformation.setLowerCorner(new double[] {1, 1}); // Sets the lower corner to {1, 1}
 *     transformation.setUpperCorner(new double[] {2, 2}); // Sets the upper corner to {2, 2}
 *     transformation.setTransformations(new ArrayList<double[]>()); // Sets the transformations to an empty list
 * </pre></blockquote>
 *
 * @author Rolf Normann Flatøy
 * @version 0.0.5
 * @since 0.0.2
 */
public class Transformation {

    private String transformationName;
    private String transformationType;
    private double[] lowerCorner;
    private double[] upperCorner;
    private List<double[]> transmformations;
    private static final Logger logger = LogManager.getLogger(Transformation.class);

    /**
     * Constructs a new {@code Transformation} with no specified name, lower corner, upper corner or transformations.
     *
     * @since 0.0.2
     */
    public Transformation() {

    }
    /**
     * Constructs a new {@code Transformation} with the specified name, lower corner, upper corner and transformations.
     *
     * @param name the name of the transformation.
     * @param lowerCorner the lower corner of the transformation.l
     * @param upperCorner the upper corner of the transformation.
     * @param transformations  the transformations of the transformation.
     *
     * @since 0.0.2
     */
    public Transformation(String name, double[] lowerCorner, double[] upperCorner, List<double[]> transformations) {
        setTransformationName(name);
        setLowerCorner(lowerCorner);
        setUpperCorner(upperCorner);
        setTransformations(transformations);
    }

    public Vector2D getMinCoords() {
        return new Vector2D(lowerCorner[0], lowerCorner[1]);
    }

    public Vector2D getMaxCoords() {
        return new Vector2D(upperCorner[0], upperCorner[1]);
    }

    public void setMaxCoords(Vector2D vector) {
        this.upperCorner = new double[] {vector.getX0(), vector.getX1()};
    }

    public void setMinCoords(Vector2D vector) {
        this.lowerCorner = new double[] {vector.getX0(), vector.getX1()};
    } 

    public List<AffineTransform2D> getAffineTransform() {
        if (transformationName.equals(ChaosGameHandler.JULIA_TRANSFORM)) {
            logger.info("Tried creating affine transformations in a julia transformation " + 
            "object.");
            throw new UnsupportedOperationException("Cannot retrieve affine transformations " + 
            "when it is a julia transformation object.");
        }
        ArrayList<AffineTransform2D> affineTransforms = new ArrayList<>();
        for (double[] transform : transmformations) {
            Matrix2D matrix = new Matrix2D(transform[0], transform[1], transform[2], transform[3]);
            Vector2D vector = new Vector2D(transform[4], transform[5]);
            affineTransforms.add(new AffineTransform2D(matrix, vector));
        }
        return affineTransforms;
    }

    /**
     * Returns the transforms of this class as juliatransforms. 
     * @return
     */
    public List<JuliaTransform> getJuliaTransforms() {
        if (!transformationType.equals(ChaosGameHandler.JULIA_TRANSFORM)) {
            logger.info("Tried creating julia transformations in an affine transformation " + 
            "object.");
            throw new UnsupportedOperationException("Cannot retrieve julia transformations " + 
            "when it is an affine transformation object.");
        }
        ArrayList<JuliaTransform> juliaTransforms = new ArrayList<>();
        for (double[] transform : transmformations) {
            juliaTransforms.add(new JuliaTransform(new Complex(transform[0], transform[1]), (int)transform[2]));
        }
        return juliaTransforms;
    }

    /**
     *
     * @return the name of the transformation
     * @since 0.0.2
     */
    public String getTransformationType() {
        return transformationType;
    }

    public String getTransformationName() {
        return transformationName;
    }

    /**
     *
     * @return the lower corner of the transformation
     * @since 0.0.2
     */
    public double[] getLowerCorner() {
        return lowerCorner;
    }

    /**
     *
     * @return the upper corner of the transformation
     * @since 0.0.2
     */
    public double[] getUpperCorner() {
        return upperCorner;
    }

    /**
     *
     * @return the transformations of the transformation
     * @since 0.0.2
     */
    public List<double[]> getTransformations() {
        return transmformations;
    }

    /**
     *
     * @param name the name of the transformation
     * @since 0.0.2
     */
    public void setTransformationName(String name) {
        this.transformationName = name;
    }

    public void setTransformationType(String type) {
        this.transformationType = type;
    }

    /**
     *
     * @param lowerCorner the lower corner of the transformation
     * @since 0.0.2
     */
    public void setLowerCorner(double[] lowerCorner) {
        this.lowerCorner = lowerCorner;
    }

    /**
     *
     * @param upperCorner the upper corner of the transformation
     * @since 0.0.2
     */
    public void setUpperCorner(double[] upperCorner) {
        this.upperCorner = upperCorner;
    }

    /**
     *
     * @param transformations the transformations of the transformation
     * @since 0.0.2
     */
    public void setTransformations(List<double[]> transformations) {
        this.transmformations = transformations;
    }

    public void setJuliaTransformations(List<JuliaTransform> transformations) {
        List<double[]> juliaTransformations = new ArrayList<>();

        for (JuliaTransform juliaTransform : transformations) {
            juliaTransformations.add(new double[]{juliaTransform.getPoint().getX0(), juliaTransform.getPoint().getX1(), 
            juliaTransform.getSign()});
        }
        this.transmformations = juliaTransformations;
    }

    public void setAffineTransforms(List<AffineTransform2D> transofrmations) {
        List<double[]> affineTransforms = new ArrayList<>();

        for (AffineTransform2D affineTransform : transofrmations) {
            Matrix2D matrix = affineTransform.getMatrix();
            affineTransforms.add(new double[]{
                matrix.getMatrix()[0][0],matrix.getMatrix()[0][1], 
                matrix.getMatrix()[1][0], matrix.getMatrix()[1][1],
                affineTransform.getVector().getX0(), affineTransform.getVector().getX1()});
        }
        this.transmformations = affineTransforms;
    }
}
