package no.ntnu.transformations;

import java.util.Random;


import no.ntnu.unitclasses.Complex;
import no.ntnu.unitclasses.Vector2D;

/**
 *
 * The {@code JuliaTransform} is a class that represents a transformation of
 * a real and an imaginary part of a complex number. The transformation can be
 * both positive and negative.
 * The class includes a point and a sign, which is used to determine the transformation
 * and method to transform a vector.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *     JuliaTransform juliaTransform = new JuliaTransform(new Complex(1, 1), 1);
 * </pre></blockquote>
 *
 *
 * @since 0.0.1
 * @version 0.0.1
 * @author Joachim Duong
 */
public class JuliaTransform extends Transform2D {
    private Complex point;
    private int sign;
    private static final String POINT_CANNOT_BE_NULL = "Point cannot be null";

    /**
     * Creates a new Julia transformation with the specified point and sign.
     * @param point the point to set, cannot be null
     * @param sign the sign to set, must be 1 or -1
     *
     * @since 0.0.1
     */
    public JuliaTransform(Complex point, int sign) {
        setPoint(point);
        setSign(sign);
    }

    /**
     * Creates a new Julia transformation with the specified point and a random sign.
     * @param point the point to set, cannot be null
     * @since 0.0.1
     */
    public JuliaTransform(Complex point) {
        setPoint(point);
        Random random = new Random();
        setSign(random.nextInt(2) == 0 ? 1 : -1);
    }

    /**
     *
     * @param point the point to set, cannot be null
     * @throws IllegalArgumentException if the specified point is null
     * @since 0.0.1
     */
    public void setPoint(Complex point) {
        if (point == null) {
            throw new IllegalArgumentException(POINT_CANNOT_BE_NULL);
        }
        this.point = point;
    }

    /**
     * set the point with a Vector2D, making the first value the real part and the second value the imaginary part
     * @param point the point to set, cannot be null
     * @since 0.0.1
     */
    public void setPoint(Vector2D point) {
        if (point == null) {
            throw new IllegalArgumentException(POINT_CANNOT_BE_NULL);
        }
        this.point = new Complex(point.getX0(), point.getX1());
    }

    /**
     *
     * @param sign the sign to set, must be 1 or -1
     * @throws IllegalArgumentException if the sign is not 1 or -1
     * @since 0.0.1
     */
    public void setSign(int sign) {
        if (sign != 1 && sign != -1) {
            throw new IllegalArgumentException("Sign must be 1 or -1");
        }
        this.sign = sign;
    }


    /**
     * Preforms the Julia transformation on the specified vector and returns the result
     * as a new Vector2D object.
     * @param vector the vector to transform
     * @return a new vector with the result of the transformation
     * @throws IllegalArgumentException if the specified vector is null
     * @since 0.0.1
     */
    @Override
    public Vector2D transform(Vector2D vector) {
        if (vector == null) {
            throw new IllegalArgumentException(POINT_CANNOT_BE_NULL);
        }
        Complex rootSum = new Complex(vector.subtract(this.point));
        Complex root = rootSum.sqrt();
        return new Vector2D(root.getX0() * sign, root.getX1() * sign);
    }

    /**
     *
     * @return the Complex point
     * @since 0.0.1
     */
    public Complex getPoint() {
        return point;
    }

    /**
     *
     * @return the sign
     * @since 0.0.1
     */
    public int getSign() {
        return sign;
    }
}
