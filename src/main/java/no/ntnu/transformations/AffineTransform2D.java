package no.ntnu.transformations;

import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.unitclasses.Vector2D;

/**
 * The {@code AffineTransform2D} class represents a 2D affine transformation.
 * All {@code AffineTransform2D}s have a matrix and a vector.
 * You can transform by using the transform method and passing a vector(point). 
 * This will return a new vector with the result of the transformation.
 * 
 * @author Rolf Normann Flatøy
 * @version 0.0.1
 * @since 0.0.1
 */
public class AffineTransform2D extends Transform2D {
    private Matrix2D matrix;
    private Vector2D vector;

    /**
     * Constructs a new {@code AffineTransform2D} with the specified matrix and vector.
     * @param matrix the matrix to be created. Cannot be null or contain NaN values
     * @param vector the vector to be created. Cannot be null or contain NaN values
     * @since 0.0.1
     */
    public AffineTransform2D(Matrix2D matrix, Vector2D vector) {
        setMatrix(matrix);
        setVector(vector);
    }

    /**
     * Constructs a new {@code AffineTransform2D} with the specified matrix.
     * Since the vector is not specified, a vector with zero values will be created. 
     * 
     * @param matrix the matrix to be created. Cannot be null or contain NaN values
     * @since 0.0.1
     */
    public AffineTransform2D(Matrix2D matrix) {
        setMatrix(matrix);
        this.vector = new Vector2D();
    }

    /**
     * Constructs a new {@code AffineTransform2D} with the specified vector.
     * Since the matrix is not specified, a matrix with zero values will be created. 
     * 
     * @param vector the vector to be created. Cannot be null or contain NaN values
     * @since 0.0.1
     */
    public AffineTransform2D(Vector2D vector) {
        this.matrix = new Matrix2D();
        setVector(vector);
    }

    /**
     * Constructs a new {@code AffineTransform2D} with no specified matrix or vector.
     * Since the matrix and vector is not specified, a matrix and vector with zero values will be created.
     * @since 0.0.1
     */
    public AffineTransform2D() {
        this.matrix = new Matrix2D();
        this.vector = new Vector2D();
    }

    /**
     *
     * @param point the point to transform, cannot be null or contain NaN values
     * @return a new vector with the result of the transformation
     * @since 0.0.1
     */
    @Override
    public Vector2D transform(Vector2D point) {
        if (point == null || Double.isNaN(point.getX1()) || Double.isNaN(point.getX0())) {
            throw new IllegalArgumentException("Point cannot be NaN");
        }

        return matrix.mulitply(point).add(vector);
    }

    /**
     * Get the matrix.
     *
     * @since 0.0.1
     */
    public Matrix2D getMatrix() {
        return matrix;
    }

    /**
     * Get the vector.
     *
     * @since 0.0.1
     */
    public Vector2D getVector() {
        return vector;
    }

    /**
     * Set the matrix with a Matrix2D.
     * 
     * @param matrix the matrix to set, cannot be null or contain NaN values
     * @since 0.0.1
     */
    public void setMatrix(Matrix2D matrix) {
        if (matrix == null) {
            throw new IllegalArgumentException("Matrix cannot be null");
        }
        if (matrix.containsNaNValues()) {
            throw new IllegalArgumentException("Matrix cannot contain NaN values");
        }
        this.matrix = matrix;
    }

    /**
     * Set the vector with a Vector2D.
     * 
     * @param vector the vector to set, cannot be null or contain NaN values
     * @since 0.0.1
     */
    public void setVector(Vector2D vector) {
        if (vector == null) {
            throw new IllegalArgumentException("Vector cannot be null");
        }
        if (vector.containsNaNValues()) {
            throw new IllegalArgumentException("Vector cannot contain NaN values");
        }
        this.vector = vector;
    }
}
