package no.ntnu.transformations;

import no.ntnu.unitclasses.Vector2D;

/**
 * The {@code Transform2D} class is an abstract class meant to be inherited by other
 * transformation classes. All classes that inherit from {@code Transform2D} must have
 * a method for transforming a vector.
 *
 * <p>Example:
 *
 * <blockquote><pre>
 *    Transform2D transform = new Transform2D(PARAMETERS);
 * </pre></blockquote>
 *
 * <p>The class {@code Transform2D} includes methods for transforming a vector.
 *
 * @author Joachim Duong
 * @version 0.0.1
 * @since 0.0.1
 */

public abstract class Transform2D {
    public abstract Vector2D transform(Vector2D point);
}
