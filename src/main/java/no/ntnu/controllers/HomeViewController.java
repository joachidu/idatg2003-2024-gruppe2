package no.ntnu.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import no.ntnu.FxApplication;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.filemanipulation.SaveManager;
import no.ntnu.infoMessages.MessageRetrieval;
import no.ntnu.infoMessages.Messages;
import no.ntnu.trackers.ControllerTracker;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;


/**
 * Controller for the HomeView.fxml file. This controller is responsible for handling the events
 * that occur in the HomeView.fxml file, which are two buttons that will take the user to the
 * AffineView.fxml and JuliaView.fxml files.
 *
 * @author Joachim Duong
 * @version 0.0.4
 * @since 0.0.4
 */
public class HomeViewController implements Initializable {
    @FXML
    Button sierpinskiButton;
    @FXML
    Button juliaButton;
    private static Logger logger = LogManager.getLogger(HomeViewController.class);

    public void sierpinskiClick(ActionEvent actionEvent) {
        Scene scene = ((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("AffineView"));
    }

    /**
     * This method is called when the user clicks the "Julia Transform" button in the HomeView.fxml
     * file. The method will change the scene to the JuliaView.fxml file.
     *
     * @param actionEvent the event that occurred
     * @since 0.0.4
     */
    public void juliaTransformClick(ActionEvent actionEvent) {
        Scene scene = ((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("JuliaView"));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (!ControllerTracker.isControllerLoaded(getClass())) {
            String transformationType = getTransformationType();
            if (transformationType != null && !transformationType.isEmpty()) {
                ChangeListener<Scene> listener = createListener(transformationType);
                sierpinskiButton.sceneProperty().addListener(listener);
            }
        }
        ControllerTracker.setControllerLoaded(getClass());
    }

    private String getTransformationType() {
        try {
            return SaveManager.getInstance().getTransformationType();
        } catch (Exception exception) {
            logger.error("Failed to get the previous savestate.", exception);
            Alert stateError = new Alert(AlertType.ERROR, 
                MessageRetrieval.getString(Messages.FAILED_TO_LOAD_PREVIOUS_STATE_ERROR.getValue()), ButtonType.OK);
            stateError.showAndWait();
        }
        return "";
    }

    private ChangeListener<Scene> createListener(String transformationType) {
        return new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
                if (newValue != null) {
                    sierpinskiButton.sceneProperty().removeListener(this);
                    if (transformationType != null && transformationType.equals(ChaosGameHandler.JULIA_TRANSFORM)) {
                        newValue.setRoot(FxApplication.loadFXML("JuliaView"));
                    } else if (transformationType != null) {
                        newValue.setRoot(FxApplication.loadFXML("AffineView"));
                    }
                }
            }
        };
    }
}
