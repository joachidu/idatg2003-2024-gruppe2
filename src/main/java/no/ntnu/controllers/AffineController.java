package no.ntnu.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import no.ntnu.chaosgame.ChaosGameDescription;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.filemanipulation.SaveManager;
import no.ntnu.infoMessages.MessageRetrieval;
import no.ntnu.infoMessages.Messages;
import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.Transform2D;
import no.ntnu.transformations.Transformation;
import no.ntnu.unitclasses.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * The AffineController class is the controller for the AffineView.fxml file. It is responsible for handling the user input
 * and updating the view. It can change parameters of the affine transformations and create new fractals.
 *
 * @author Joachim Duong
 * @version 0.0.4
 * @since 0.0.3
 */
public class AffineController extends ViewController  {
    @FXML
    private Label logLabel2;
    @FXML
    private TextField nameField;
    @FXML
    private TextField x0Field;
    @FXML
    private TextField y0Field;
    @FXML
    private TextField x1Field;
    @FXML
    private TextField y1Field;
    @FXML
    private TextField v0Field;
    @FXML
    private TextField v1Field;
    @FXML
    private Label addTransformationFeedback;
    @FXML
    private Label transformationLabel;

    @FXML
    private Label x0Label;
    @FXML
    private Label y0Label;
    @FXML
    private Label x1Label;
    @FXML
    private Label y1Label;
    @FXML
    private Label v0Label;
    @FXML
    private Label v1Label;
    private int transformationIndex = 0;
    private List<Transform2D> tempTransforms = new ArrayList<>();

    /**
     * This method is called when the user clicks the Sierpinski button. It creates a chaosGameDescription
     * with the preset Sierpinski triangle and initializes the chaos game with the amount of steps specified by the user.
     * It then updates the labels and draws the canvas.
     *
     * @param actionEvent the automatic action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    @FXML
    public void sierpinskiClick(ActionEvent actionEvent) {
        getChaosGameHandler().setSierpinskiDescription((int)getCanvas().getWidth(), (int)getCanvas().getHeight());
        getChaosGameHandler().setSteps(getSteps());
        getChaosGameHandler().init(getSteps()); // The amount of steps, get this from the slider
        updateLabels();
        drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
    }

    /**
     * This method is called when the user clicks the Barnsley Fern button. It creates a chaosGameDescription
     * with the preset Barnsley Fern and initializes the chaos game with the amount of steps specified by the user.
     * It then updates the labels and draws the canvas.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    @FXML
    public void barnsleyFernClick(ActionEvent actionEvent) {
        getChaosGameHandler().setBarnsleyFernDescription();
        getChaosGameHandler().setSteps(getSteps());
        getChaosGameHandler().init(getSteps()); // The amount of steps, get this from the slider
        updateLabels();
        drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
    }

    /**
     *
     * @return the transformation in the transformation list that is currently selected
     */
    private AffineTransform2D getCurrentTransform() {
        return (AffineTransform2D) getChaosGameHandler().getChaosGameDescription().getTransforms().get(transformationIndex);
    }

    /**
     * This method displays the next transformation in the transformation list.
     * It increments the transformation index and updates the labels.
     * This method is called when the user clicks the next button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void nextTransformation(ActionEvent actionEvent) {
        try {
        transformationIndex++;
        if (transformationIndex >= getChaosGameHandler().getChaosGameDescription().getTransforms().size()) {
            transformationIndex = 0;
        }
        updateLabels();
        } catch (NullPointerException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.CHOOSE_OR_CREATE_FRACTAL.getValue()));
            transformationIndex--;
        }
    }

    /**
     * This method sets all the labels to 0 and the transformation label to "No fractal chosen".
     * It is called when do fractal is chosen.
     */
     @Override
    public void nanLabel() {
        getMinCordLabel1().setText("0");
        getMinCordLabel2().setText("0");
        getMaxCordLabel1().setText("0");
        getMaxCordLabel2().setText("0");
        x0Label.setText("0");
        y0Label.setText("0");
        x1Label.setText("0");
        y1Label.setText("0");
        v0Label.setText("0");
        v1Label.setText("0");
        transformationLabel.setText(MessageRetrieval.getString(Messages.NO_FRACTAL_CHOSEN_WARNING.getValue()));
    }

    /**
     * This method updates all the labels in the view.
     * IT calls its super method and then updates the labels for the affine transformations.
     *
     * @since 0.0.3
     */
    @Override
    public void updateLabels() {
        try {
            super.updateLabels();
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            x0Label.setText(String.valueOf(getCurrentTransform().getMatrix().getMatrix()[0][0]));
            y0Label.setText(String.valueOf(getCurrentTransform().getMatrix().getMatrix()[0][1]));
            x1Label.setText(String.valueOf(getCurrentTransform().getMatrix().getMatrix()[1][0]));
            y1Label.setText(String.valueOf(getCurrentTransform().getMatrix().getMatrix()[1][1]));
            v0Label.setText(String.valueOf(getCurrentTransform().getVector().getX0()));
            v1Label.setText(String.valueOf(getCurrentTransform().getVector().getX1()));
            transformationLabel.setText("Transformation " + (transformationIndex + 1) + " of " + chaosGameDescription.getTransforms().size());
        }
        catch (NullPointerException | IndexOutOfBoundsException e){
            nanLabel();
            getLogLabel().setText(MessageRetrieval.getString(Messages.CHOOSE_OR_CREATE_FRACTAL.getValue()));
        }
    }

    /**
     * Decreases the x0 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease x0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseMatrixX0Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[0][0] = newMatrix[0][0] - 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the x0 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase x0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseMatrixX0Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[0][0] = newMatrix[0][0] + 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Decreases the y0 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease y0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseMatrixY0Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[0][1] = newMatrix[0][1] - 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the y0 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase y0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseMatrixY0Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[0][1] = newMatrix[0][1] + 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Decreases the x1 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease x1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseMatrixX1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[1][0] = newMatrix[1][0] - 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the x1 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase x1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseMatrixX1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[1][0] = newMatrix[1][0] + 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Decreases the y1 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease y1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseMatrixY1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[1][1] = newMatrix[1][1] - 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the y1 value of the current transformation's matrix by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase y1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseMatrixY1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            double[][] newMatrix = currentTransform.getMatrix().getMatrix();
            newMatrix[1][1] = newMatrix[1][1] + 0.1;
            currentTransform.getMatrix().setMatrix(newMatrix);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Decreases the v0 value of the current transformation's vector by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease v0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseV0Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            Vector2D newVector = currentTransform.getVector();
            newVector.setX0(newVector.getX0() - 0.1);
            currentTransform.setVector(newVector);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the v0 value of the current transformation's vector by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase v0 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseVC0licked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            Vector2D newVector = currentTransform.getVector();
            newVector.setX0(newVector.getX0() + 0.1);
            currentTransform.setVector(newVector);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Decreases the v1 value of the current transformation's vector by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the decrease v1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void decreaseV1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            Vector2D newVector = currentTransform.getVector();
            newVector.setX1(newVector.getX1() - 0.1);
            currentTransform.setVector(newVector);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }

    }

    /**
     * Increases the v1 value of the current transformation's vector by 0.1.
     * It then updates the labels and restarts the chaos game.
     * If no transformation is chosen, it displays a message to the user.
     * This method is called when the user clicks the increase v1 button.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void increaseV1Clicked(ActionEvent actionEvent) {
        try {
            AffineTransform2D currentTransform = getCurrentTransform();
            Vector2D newVector = currentTransform.getVector();
            newVector.setX1(newVector.getX1() + 0.1);
            currentTransform.setVector(newVector);
            updateLabels();
            restartChaosGame(false);
        }
        catch (NullPointerException | IndexOutOfBoundsException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Creates a new transformation with the values specified by the user.
     * It creates a new AffineTransform2D object and adds it to the list of transformations.
     * It then clears the inputs in case the user wants to add another transformation.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void addTransformation(ActionEvent actionEvent) {
        try {
            String x0 = x0Field.getText();
            String y0 = y0Field.getText();
            String x1 = x1Field.getText();
            String y1 = y1Field.getText();
            String v0 = v0Field.getText();
            String v1 = v1Field.getText();
            Vector2D v = new Vector2D(Double.parseDouble(v0), Double.parseDouble(v1));
            Matrix2D matrix = new Matrix2D(Double.parseDouble(x0), Double.parseDouble(y0), Double.parseDouble(x1), Double.parseDouble(y1));
            AffineTransform2D affineTransform = new AffineTransform2D(matrix, v);
            tempTransforms.add(affineTransform);
            clearTransformationInput();
            addTransformationFeedback.setText(MessageRetrieval.getString(Messages.TRANSFORMATION_ADDED_INFO.getValue()));
        }
        catch (NullPointerException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.MISSING_FIELDS_WARNING.getValue()));
        }
        catch (NumberFormatException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_NUMBER_INPUT_WARNING.getValue()));
        }
    }

    /**
     * This method swaps the Parameterbox and the createBox.
     * it calls it super method and sets the transformation index to 0.
     *
     * @since 0.0.3
     */
    @Override
    public void swapHBox() {
        super.swapHBox();
        transformationIndex = 0;
    }

    /**
     * This method clears all the input fields for the affine transformations.
     *
     * @since 0.0.3
     */
    private void clearTransformationInput() {
        x0Field.clear();
        y0Field.clear();
        x1Field.clear();
        y1Field.clear();
        v0Field.clear();
        v1Field.clear();

    }

    /**
     * Creates a new fractal with the values specified by the user.
     * It takes in the lower and upper corner values from the text fields and the previously added
     * transformations. It then creates a new ChaosGameDescription object and initializes the chaos game with it.
     * It then updates the labels and clears the transformation list.
     *
     * @param actionEvent the action event that is passed when the button is clicked.
     * @since 0.0.3
     */
    public void createFractal(ActionEvent actionEvent) {
        try {
            Vector2D lowerCorner = new Vector2D(Double.parseDouble(getMinCordField1().getText()), Double.parseDouble(getMinCordField2().getText()));
            Vector2D upperCorner = new Vector2D(Double.parseDouble(getMaxCordField1().getText()), Double.parseDouble(getMaxCordField2().getText()));

            ChaosGameDescription chaosGameDescription = new ChaosGameDescription(lowerCorner, upperCorner, new ArrayList<>(tempTransforms), ChaosGameHandler.AFFINE_TRANSFORM);
            getChaosGameHandler().setChaosGameDescription(chaosGameDescription);
            restartChaosGame(false);
            updateLabels();
            tempTransforms.clear();
            swapHBox();
            getLogLabel().setText(MessageRetrieval.getString(Messages.FRACTAL_CREATED_INFO.getValue()));
        }
        catch (NumberFormatException e){
            getLogLabel2().setText(MessageRetrieval.getString(Messages.INVALID_NUMBER_INPUT_WARNING.getValue()));
        }
        catch (IllegalArgumentException e) {
            if (e.getMessage().contains("bound must be positive")) {
                getLogLabel2().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
            } else {
                getLogLabel2().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
            }
        }
        catch (NullPointerException e){
            getLogLabel2().setText(MessageRetrieval.getString(Messages.MISSING_FIELDS_WARNING.getValue()));
        }
        updateLabels();
    }

    public void save() {
        String fractalName = getFractalNameField().getText();
        if (fractalName.isBlank()) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NAME_IS_EMPTY_OR_WHITE_SPACE_WARNING.getValue()));
            return;
        }
        Vector2D maxCoords;
        Vector2D minCoords;
        List<Transform2D> transforms;
        try {
            maxCoords = getChaosGameHandler().getChaosGameDescription().getMaxCoords();
            minCoords = getChaosGameHandler().getChaosGameDescription().getMinCoords();
            transforms = getChaosGameHandler().getChaosGameDescription().getTransforms();
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.INCOMPLETE_FRACTAL_WHEN_SAVING_WARNING.getValue()));
            return;
        }
        if (maxCoords == null || minCoords == null || transforms == null) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.INCOMPLETE_FRACTAL_WHEN_SAVING_WARNING.getValue()));
            return;
        }
        Transformation currentFractal = new Transformation();
        currentFractal.setTransformationName(fractalName);
        currentFractal.setTransformationType(ChaosGameHandler.AFFINE_TRANSFORM);
        currentFractal.setMaxCoords(maxCoords);
        currentFractal.setMinCoords(minCoords);
        ArrayList<AffineTransform2D> affineTransforms = new ArrayList<>();
        for (Transform2D transform : transforms) {
            affineTransforms.add((AffineTransform2D)transform);
        }
        currentFractal.setAffineTransforms(affineTransforms);
        SaveManager.getInstance().saveFractal(currentFractal);
        updateLabels();
    }

    /**
     * Populates the fractal list with the saved Julia fractals.
     * It uses the saveManager to get the saved Julia fractals and then adds them to the list.
     * It then iterates through the lists and adds the names of the fractals to the list.
     *
     * @since 0.0.5
     */
    @Override
    public void populateFractalList(){
        SaveManager saveManager = SaveManager.getInstance();
        List<Path> fractals = saveManager.getFractalNamesByType(true);
        List<String> displayList = new ArrayList<>();
        for (Path path : fractals) {
            String filename = path.getFileName().toString().substring(1);
            displayList.add(filename);
        }
        getFractalList().getItems().clear();
        getFractalList().getItems().addAll(displayList);
    }

    /**
     * Loads a fractal from the saved fractals.
     * It uses the saveManager to load the fractal and then sets the chaos game description to the loaded fractal's data.
     * It then runs the chaos game and updates the labels and canvas.
     *
     * @param fractalName the name of the fractal
     * @since 0.0.4
     */
    @Override
    public void loadFractalData(String fractalName) {
        SaveManager saveManager = SaveManager.getInstance();
        String searchName = "a" + fractalName;
        try {
        Transformation fractal = saveManager.loadFractal(searchName);
        getChaosGameHandler().setCustomDescription(fractal, true);
        getChaosGameHandler().setSteps(getSteps());
        getChaosGameHandler().init(getSteps()); // The amount of steps, get this from the slider
        updateLabels();
        drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.UNABLE_TO_LOAD_FRACTAL_ERROR.getValue()));
        }
    }
}
