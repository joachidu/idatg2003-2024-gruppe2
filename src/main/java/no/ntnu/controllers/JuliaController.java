package no.ntnu.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import no.ntnu.chaosgame.ChaosGameDescription;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.filemanipulation.SaveManager;
import no.ntnu.infoMessages.MessageRetrieval;
import no.ntnu.infoMessages.Messages;
import no.ntnu.transformations.JuliaTransform;
import no.ntnu.transformations.Transformation;
import no.ntnu.unitclasses.Complex;
import no.ntnu.transformations.Transform2D;
import no.ntnu.unitclasses.Vector2D;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * The JuliaController class is the controller for the JuliaView.fxml file. It is responsible for handling the user input
 * and updating the view. It also has method to change parameters of the Julia fractal and create a new Julia fractal.
 *
 * @author Joachim Duong
 * @version 0.0.4
 * @since 0.0.3
 */
public class JuliaController extends ViewController {
    @FXML
    public Label parameterFieldReal;
    @FXML
    public Label parameterFieldImaginary;
    @FXML
    private TextField c0Field;
    @FXML
    private TextField c1Field;

    /**
     * Sets the description of the chaos game to be a Julia fractal.
     * It the initializes the chaos game and updates the labels and canvas.
     * It is called when the user clicks the Julia button.
     *
     * @param event the event that triggered the method
     * @since 0.0.4
     */
    @FXML
    private void juliaTransformClick(ActionEvent event) {
        getChaosGameHandler().setJuliaDescription((int)getCanvas().getWidth(), (int)getCanvas().getHeight());
        getChaosGameHandler().setSteps(getSteps());
        getChaosGameHandler().init(getSteps());
        updateLabels();
        drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
    }

    /**
     * Updates the labels with the current values of the Julia fractal.
     * It firsts calls the super method that updates the common labels
     * and then updates the specific labels for the Julia fractal.
     *
     * @since 0.0.4
     */
    @Override
    public void updateLabels() {
        super.updateLabels();
        try {
            parameterFieldReal.setText(String.valueOf(getJuliaTransform().getPoint().getX0()));
            parameterFieldImaginary.setText(String.valueOf(getJuliaTransform().getPoint().getX1()));
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     *
     * @return the Julia transform of the chaos game.
     * @since 0.0.4
     */
    public JuliaTransform getJuliaTransform() {
        try {
            return (JuliaTransform) getChaosGameHandler().getChaosGameDescription().getTransforms().getFirst();
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
            return null;
        }
    }

    /**
     * Reduces the current value of the first parameter of the Julia fractal.
     * It then updates the labels and restarts the chaos game.
     * It is called when the user clicks the decrease button for the first parameter.
     *
     * @param actionEvent the event that triggered the method
     * @since 0.0.4
     */
    public void decreaseC0Clicked(ActionEvent actionEvent) {
        try {
            JuliaTransform currentTransform = (JuliaTransform) getChaosGameHandler().getChaosGameDescription().getTransforms().getFirst();
            Complex currentC = currentTransform.getPoint();
            currentC.setX0(currentC.getX0() - 0.1);
            updateLabels();
            restartChaosGame(false);
        }
        catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the current value of the first parameter of the Julia fractal.
     * It then updates the labels and restarts the chaos game.
     * It is called when the user clicks the increase button for the first parameter.
     *
     * @param actionEvent the event that triggered the method
     * @since 0.0.4
     */
    public void increaseC0Clicked(ActionEvent actionEvent) {
        try {
            JuliaTransform currentTransform = (JuliaTransform) getChaosGameHandler().getChaosGameDescription().getTransforms().getFirst();
            Complex currentC = currentTransform.getPoint();
            currentC.setX0(currentC.getX0() + 0.1);
            updateLabels();
            restartChaosGame(false);
        }
        catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Reduces the current value of the second parameter of the Julia fractal.
     * It then updates the labels and restarts the chaos game.
     * It is called when the user clicks the decrease button for the second parameter.
     *
     * @param actionEvent the event that triggered the method
     * @since 0.0.4
     */
    public void decreaseC1Clicked(ActionEvent actionEvent) {
        try {
            JuliaTransform currentTransform = (JuliaTransform) getChaosGameHandler().getChaosGameDescription().getTransforms().getFirst();
            Complex currentC = currentTransform.getPoint();
            currentC.setX1(currentC.getX1() - 0.1);
            updateLabels();
            restartChaosGame(false);
        }
        catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Increases the current value of the second parameter of the Julia fractal.
     * It then updates the labels and restarts the chaos game.
     * It is called when the user clicks the increase button for the second parameter.
     *
     * @param actionEvent the event that triggered the method
     * @since 0.0.4
     */
    public void increaseC1Clicked(ActionEvent actionEvent) {
        try {
            JuliaTransform currentTransform = (JuliaTransform) getChaosGameHandler().getChaosGameDescription().getTransforms().getFirst();
            Complex currentC = currentTransform.getPoint();
            currentC.setX1(currentC.getX1() + 0.1);
            updateLabels();
            restartChaosGame(false);
        }
        catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
    }

    /**
     * Creates a new fractal using the values in the text fields.
     * It parses all the values and creates a new Julia fractal with the specified values.
     * It adds both the positive and negative Julia transform to the chaos game description.
     * It then restarts the chaos game and updates the labels.
     *
     * @param actionEvent the event that triggered the method
     * @since 0.0.4
     */
    public void createFractal(ActionEvent actionEvent) {
        try {
            Vector2D lowerCorner = new Vector2D(Double.parseDouble(getMinCordField1().getText()), Double.parseDouble(getMinCordField2().getText()));
            Vector2D upperCorner = new Vector2D(Double.parseDouble(getMaxCordField1().getText()), Double.parseDouble(getMaxCordField2().getText()));
            ArrayList<Transform2D> tempTransforms = new ArrayList<>();
            JuliaTransform juliaTransform = new JuliaTransform(new Complex(Double.parseDouble(c0Field.getText()), Double.parseDouble(c1Field.getText())), 1);
            JuliaTransform juliaTransform2 = new JuliaTransform(new Complex(Double.parseDouble(c0Field.getText()), Double.parseDouble(c1Field.getText())), -1);
            tempTransforms.add(juliaTransform);
            tempTransforms.add(juliaTransform2);
            ChaosGameDescription chaosGameDescription = new ChaosGameDescription(lowerCorner, upperCorner, new ArrayList<>(tempTransforms), ChaosGameHandler.JULIA_TRANSFORM);
            getChaosGameHandler().setChaosGameDescription(chaosGameDescription);
            restartChaosGame(false);
            updateLabels();
            swapHBox();
            getLogLabel().setText(MessageRetrieval.getString(Messages.FRACTAL_CREATED_INFO.getValue()));

        }
        catch (NumberFormatException e){
            getLogLabel2().setText(MessageRetrieval.getString(Messages.INVALID_NUMBER_INPUT_WARNING.getValue()));
        }
        catch (IllegalArgumentException e) {
            getLogLabel2().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
        catch (NullPointerException e){
            getLogLabel2().setText(MessageRetrieval.getString(Messages.MISSING_FIELDS_WARNING.getValue()));
        }
        updateLabels();

    }

    public void save() {
        String fractalName = getFractalNameField().getText();
        if (fractalName.isBlank()) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NAME_IS_EMPTY_OR_WHITE_SPACE_WARNING.getValue()));
            return;
        }
        Vector2D maxCoords;
        Vector2D minCoords;
        List<Transform2D> transforms;
        try {
            maxCoords = getChaosGameHandler().getChaosGameDescription().getMaxCoords();
            minCoords = getChaosGameHandler().getChaosGameDescription().getMinCoords();
            transforms = getChaosGameHandler().getChaosGameDescription().getTransforms();
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.INCOMPLETE_FRACTAL_WHEN_SAVING_WARNING.getValue()));
            return;
        }
        if (maxCoords == null || minCoords == null || transforms == null) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.INCOMPLETE_FRACTAL_WHEN_SAVING_WARNING.getValue()));
            return;
        }
        Transformation currentFractal = new Transformation();
        currentFractal.setTransformationName(fractalName);
        currentFractal.setTransformationType(ChaosGameHandler.JULIA_TRANSFORM);
        currentFractal.setMaxCoords(maxCoords);
        currentFractal.setMinCoords(minCoords);
        ArrayList<JuliaTransform> juliaTransforms = new ArrayList<>();
        for (Transform2D transform : getChaosGameHandler().getChaosGameDescription().getTransforms()) {
            juliaTransforms.add((JuliaTransform)transform);
        }
        currentFractal.setJuliaTransformations(juliaTransforms);
        SaveManager.getInstance().saveFractal(currentFractal);
        updateLabels();
    }

    /**
     * Populates the fractal list with the saved Julia fractals.
     * It uses the saveManager to get the saved Julia fractals and then adds them to the list.
     * It then iterates through the lists and adds the names of the fractals to the list.
     *
     * @since 0.0.5
     */
    @Override
    public void populateFractalList(){
        SaveManager saveManager = SaveManager.getInstance();
        List<Path> fractalList = saveManager.getFractalNamesByType(false);
        try {
            List<String> displayList = new ArrayList<>();
            for (Path path : fractalList) {
                String filename = path.getFileName().toString().substring(1);
                displayList.add(filename);
            }
            getFractalList().getItems().clear();
            getFractalList().getItems().addAll(displayList);
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_AFFINE_FRACTALS_SAVED_INFO.getValue()));
        }
    }

    /**
     * Loads a fractal from the saved fractals.
     * It uses the saveManager to load the fractal and then sets the chaos game description to the loaded fractal's data.
     * It then runs the chaos game and updates the labels and canvas.
     *
     * @param fractalName the name of the fractal
     * @since 0.0.4
     */
    @Override
    public void loadFractalData(String fractalName){
        SaveManager saveManager = SaveManager.getInstance();
        String searchName = "j" + fractalName;
        try {
            Transformation fractal = saveManager.loadFractal(searchName);
            getChaosGameHandler().setCustomDescription(fractal, false);
            getChaosGameHandler().setSteps(getSteps());
            getChaosGameHandler().init(getSteps());
            updateLabels();
            drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.UNABLE_TO_LOAD_FRACTAL_ERROR.getValue()));
        }
    }
}
