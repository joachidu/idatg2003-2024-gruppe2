package no.ntnu.controllers;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import no.ntnu.FxApplication;
import no.ntnu.chaosgame.ChaosGameDescription;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.infoMessages.MessageRetrieval;
import no.ntnu.infoMessages.Messages;
import no.ntnu.trackers.ControllerTracker;
import no.ntnu.unitclasses.Vector2D;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.FileNotFoundException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This is a controller class for displaying different fractals and their properties.
 * This is meant to be a class that is inherited by other classes that want to display fractals.
 * It shows all the common properties like min and max cords, steps and the canvas.
 * It also has instances of the ChaosGameHandler class to handle the fractals.
 * The class includes methods for updating the labels, drawing the canvas, restarting the chaos game,
 * drawing the fractal, increasing and decreasing the min and max cords and switching between the parameter and creation box.
 *
 * @author Joachim Duong
 * @version 0.0.4
 * @since 0.0.4
 */
public abstract class ViewController implements Initializable {
    @FXML
    private ListView<String> fractalList;
    @FXML
    private TextField minCordField1;
    @FXML
    private TextField maxCordField1;
    @FXML
    private TextField minCordField2;
    @FXML
    private TextField maxCordField2;
    @FXML
    private Label minCordLabel1;
    @FXML
    private Label maxCordLabel1;
    @FXML
    private Label minCordLabel2;
    @FXML
    private Label maxCordLabel2;
    private final ChaosGameHandler chaosGameHandler;
    @FXML
    private Label stepLabel;
    @FXML
    private TextField stepField;
    @FXML
    private Canvas canvas;
    @FXML
    private Label logLabel;
    @FXML
    private Label logLabel2;
    @FXML
    private VBox parameterBox;
    @FXML
    private VBox creationBox;
    @FXML
    private TextField nameField;
    @FXML
    private VBox canvasVBox;
    private boolean isResizing = false;
    private static Logger logger = LogManager.getLogger(ViewController.class);

    /**
     * Constructor for the ViewController class
     *
     * @since 0.0.4
     */
    protected ViewController() {
        chaosGameHandler = new ChaosGameHandler();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setUpCanvasDimensions();
        ChangeListener<Scene> listener = getSceneListener();
        // Add the listener
        stepField.sceneProperty().addListener(listener);
    }

    public TextField getFractalNameField() {
        return nameField;
    }

    private void load(Stage currentStage) {
        loadPreviousState(currentStage);
        ControllerTracker.setControllerLoaded(ViewController.class);
        setEventHandler();
        populateFractalList();
    }

    private void setUpCanvasDimensions() {
        canvas.widthProperty().bind(canvasVBox.widthProperty());
        canvas.heightProperty().bind(canvasVBox.heightProperty());
    }

    private ChangeListener<Scene> getSceneListener() {
        // We want to add a shutdown hook. To do that we need the stage. 
        // We can get this from the scene, and we can get the scene from 
        // any UI field. The UI fields are not immediately inside the scene, so we add a listener 
        // to detect when it gets added.
        return new ChangeListener<Scene>() {
            @Override
            public void changed(ObservableValue<? extends Scene> obs, Scene oldScene, Scene newScene) {
                if (newScene != null) {
                    // If the window is not null, that means the stage is accessible and we just add the listener.
                    if (newScene.getWindow() != null) {
                        Stage currentStage = ((Stage) stepField.getScene().getWindow());
                        load(currentStage);
                        setResizeListener(currentStage);
                        setStageProperties(currentStage);
                    } else {
                        // If the window is null, that means the scene hasn't been added to the stage
                        // So we add a listener for that as well
                        newScene.windowProperty().addListener(new ChangeListener<Window>() {
                            @Override
                            public void changed(ObservableValue<? extends Window> observable, Window oldValue,
                                Window newValue) {
                                if (newValue != null) {
                                    // Remove the listener as we don't need it anymore
                                    newScene.windowProperty().removeListener(this);
                                    Stage currentStage = ((Stage) stepField.getScene().getWindow());
                                    load(currentStage);
                                    setResizeListener(currentStage);
                                    setStageProperties(currentStage);
                                }
                            }
                        });
                        // Make the listener remove itself as it is no longer needed.
                        stepField.sceneProperty().removeListener(this);
                    }
                }
            }
        };
    }

    private void setStageProperties(Stage stage) {
        stage.setOnCloseRequest(ViewController.this::save);
        stage.setMinHeight(550);
        stage.setMinWidth(1080);
    }

    private void setResizeListener(Stage stage) {
        stage.widthProperty().addListener((obs, oldval, newval) -> resizeRedrawManager(stage));
        stage.heightProperty().addListener((obs, oldval, newval) -> resizeRedrawManager(stage));
    }

    private void resizeRedrawManager(Stage stage) {
        if (!isResizing) {
            isResizing = true;
            Timer timer = new Timer();
            TimerTask task = new TimerTask() {
                double previousWidth = 0;
                double previousHeight = 0;
                @Override
                public void run() {
                    if (stage.getWidth() == previousWidth && stage.getHeight() == previousHeight) {
                        this.cancel();
                        isResizing = false;
                        Platform.runLater(() -> restartChaosGame(true));
                    }
                    previousHeight = stage.getHeight();
                    previousWidth = stage.getWidth();
                }
            };
            timer.schedule(task, 400, 400);
        }
    }

    private void loadPreviousState(Stage stage) {
        boolean isAffine = this instanceof AffineController;
        try {
            if (getChaosGameHandler().loadSaveState(isAffine) && !ControllerTracker.isControllerLoaded(ViewController.class)) {
                // We need to queue this as some elements might not have loaded in yet.
                stage.setWidth(chaosGameHandler.getPreviousSaveState().getWindowWidth());
                stage.setHeight(chaosGameHandler.getPreviousSaveState().getWindowHeight());
                getChaosGameHandler().getCanvas().setPoints(chaosGameHandler.getPreviousSaveState().getAllPoints());
                Platform.runLater(this::updateLabels);
                Platform.runLater(()-> drawCanvas(getChaosGameHandler().getCanvas().getCanvas()));
            }
        } catch (Exception exception) {
            Alert failedLoadingStateAlert = new Alert(AlertType.ERROR, 
                MessageRetrieval.getString(Messages.FAILED_TO_LOAD_PREVIOUS_STATE_ERROR.getValue()),
                ButtonType.OK);
            failedLoadingStateAlert.showAndWait();
        }
    }

    private void save(Event closingEvent) {
        logger.info("Saving chaos game");
        try {
            Stage stage = (Stage)maxCordField1.getScene().getWindow();
            getChaosGameHandler().save(stage.getWidth(), stage.getHeight());
        } catch (FileNotFoundException fileNotFoundException) {
            logger.error("Failed to save the chaos game.", fileNotFoundException);
            Alert a = new Alert(AlertType.ERROR, MessageRetrieval.getString(Messages.FAILED_TO_CREATE_SAVE_FILE_ERROR.getValue()),
            ButtonType.YES, ButtonType.NO);
            if (a.showAndWait().orElse(ButtonType.CANCEL) != ButtonType.YES) {
                closingEvent.consume();
            }
        }
        catch (IOException ioException) {
            logger.error("Failed to save the chaos game.", ioException);
            Alert a = new Alert(AlertType.ERROR, MessageRetrieval.getString(Messages.FAILED_TO_SAVE_ERROR.getValue()),
            ButtonType.YES, ButtonType.NO);
            if (a.showAndWait().orElse(ButtonType.CANCEL) != ButtonType.YES) {
                closingEvent.consume();
            }
        }

    }

    /**
     *
     * @return the instance of the ChaosGameHandler class.
     * @since 0.0.4
     */
    public ChaosGameHandler getChaosGameHandler() {
        return chaosGameHandler;
    }

    /**
     *
     * @return the amount of steps
     * @since 0.0.4
     */
    public int getSteps() {
        return chaosGameHandler.getSteps();
    }

    /**
     * Updates all the labels in the view with the current values of the fractal.
     * If the ChaosGameDescription is null, it will set the labels to 0 and display a message.
     *
     * @since 0.0.4
     */
    public void updateLabels() {
        try {
            ChaosGameDescription chaosGameDescription = chaosGameHandler.getChaosGameDescription();
            minCordLabel1.setText(String.valueOf(chaosGameDescription.getMinCoords().getX0()));
            minCordLabel2.setText(String.valueOf(chaosGameDescription.getMinCoords().getX1()));
            maxCordLabel1.setText(String.valueOf(chaosGameDescription.getMaxCoords().getX0()));
            maxCordLabel2.setText(String.valueOf(chaosGameDescription.getMaxCoords().getX1()));
            stepLabel.setText(MessageRetrieval.getString(Messages.STEP_INPUT_FIELD_PREFIX.getValue()) + 
                String.valueOf(getChaosGameHandler().getSteps()));
            stepField.setText(String.valueOf(getChaosGameHandler().getSteps()));
            populateFractalList();
        }
        catch (NullPointerException e){
            nanLabel();
            logLabel.setText(MessageRetrieval.getString(Messages.SELECT_A_FRACTAL_FIRST_WARNING.getValue()));
        }
    }

    /**
     * Sets the min cords to 0 if the ChaosGameDescription is null.
     *
     * @since 0.0.4
     */
    public void nanLabel() {
        minCordLabel1.setText("0");
        minCordLabel2.setText("0");
        maxCordLabel1.setText("0");
        maxCordLabel2.setText("0");
    }

    /**
     * Updates the current step amount the user has inputted.
     * If the input is invalid, it will set the label to "Invalid input".
     * If the input is empty, it will set the steps to 0.
     * Else if the input is a valid number, it will set the steps and
     * update the label in real time.
     *
     * @param keyEvent the key event that is triggered
     * @since 0.0.4
     */
    public void stepFieldPressed(KeyEvent keyEvent) {
        String stepInput = stepField.getText();
        if (stepInput.length() == 0 && !keyEvent.getText().matches("\\d")) {
            stepLabel.setText(MessageRetrieval.getString(Messages.STEP_INPUT_FIELD_PREFIX.getValue()) + "0");
            chaosGameHandler.setSteps(0);
        } else if (stepInput.matches("\\d+") || keyEvent.getCode() == KeyCode.BACK_SPACE || keyEvent.getText().matches("[\\d]")) {
            if (keyEvent.getText().matches("[\\d]")) {
                int steps = Integer.parseInt(stepInput + keyEvent.getText());
                chaosGameHandler.setSteps(steps);
                stepLabel.setText(MessageRetrieval.getString(Messages.STEP_INPUT_FIELD_PREFIX.getValue()) + steps);
            } else if (keyEvent.getCode() == KeyCode.BACK_SPACE) {
                int steps = Integer.parseInt(stepInput);
                chaosGameHandler.setSteps(steps);
                stepLabel.setText(MessageRetrieval.getString(Messages.STEP_INPUT_FIELD_PREFIX.getValue()) + steps);
            } else if (keyEvent.getText().length() != 0) {
                stepLabel.setText(MessageRetrieval.getString(Messages.INVALID_NUMBER_INPUT_WARNING.getValue()));
            }
        }
    }

    /**
     * Draws the canvas that is to be displayed.
     * It iterates through the canvasArray from the chaosGameHandler and draws the pixels on the canvas.
     *
     * @since 0.0.4
     */
    public void drawCanvas(int[][] canvasArray) {
        GraphicsContext gc = getCanvas().getGraphicsContext2D();
        gc.clearRect(0, 0, getCanvas().getWidth(), getCanvas().getHeight());
        for (int i = 0; i < canvasArray.length; i++) {
            for (int j = 0; j < canvasArray[0].length; j++) {
                if (canvasArray[i][j] != 0) {
                    int occurances = chaosGameHandler.getCanvas().getOccurances(new Vector2D(i,j));
                    int red = limitValue((occurances - 1) * 50, 0, 255); 
                    int blue = limitValue((occurances - 5) * 5, 0, 255);
                    gc.setFill(Color.rgb(red, blue, 0));
                    gc.fillRect((double)j * 1, (double)i * 1, 2, 2);
                }
            }
        }
    }

    private int limitValue(int value, int min, int max) {
        if (value > max) {
            return max;
        } else if (value < min) {
            return min;
        } else {
            return value;
        }
    }

    /**
     * Restarts the chaos game with the current chaos game description.
     * It also updates the labels and draws the canvas.
     *
     * @since 0.0.4
     */
    public void restartChaosGame(boolean reRun){
        try {
            logger.info("Restarting chaos game");
            getChaosGameHandler().createChaosGame((int)getCanvas().getWidth(), (int)getCanvas().getHeight(), reRun);
            if (reRun) {
                getChaosGameHandler().reRun();
            } else {
                getChaosGameHandler().init(getChaosGameHandler().getSteps());
            }
            updateLabels();
            drawCanvas(getChaosGameHandler().getCanvas().getCanvas());
        }
        catch (NullPointerException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.CHOOSE_OR_CREATE_FRACTAL.getValue()));
        }
    }

    /**
     * Restarts the chaos game with the current chaos game description.
     * This method is called when the draw button is clicked.
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void draw(ActionEvent actionEvent) {
        try {
            restartChaosGame(false);
        }
        catch (NullPointerException | IllegalArgumentException e ) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.CHOOSE_OR_CREATE_FRACTAL.getValue()));
        }

    }

    /**
     *
     * @return the minCordLabel1
     * @since 0.0.4
     */
    public Label getMinCordLabel1() {
        return minCordLabel1;
    }

    /**
     *
     * @return the maxCordLabel1
     * @since 0.0.4
     */
    public Label getMaxCordLabel1() {
        return maxCordLabel1;
    }

    /**
     *
     * @return the minCordLabel2
     * @since 0.0.4
     */
    public Label getMinCordLabel2() {
        return minCordLabel2;
    }

    /**
     *
     * @return the maxCordLabel2
     * @since 0.0.4
     */
    public Label getMaxCordLabel2() {
        return maxCordLabel2;
    }

    /**
     *
     * @return the logLabel
     * @since 0.0.4
     */
    public Label getLogLabel() {
        return logLabel;
    }

    /**
     *
     * @return the canvas
     * @since 0.0.4
     */
    public Canvas getCanvas() {
        return canvas;
    }

    /**
     *
     * @return the minCordField1
     * @since 0.0.4
     */
    public TextField getMinCordField1() {
        return minCordField1;
    }

    /**
     *
     * @return the maxCordField1
     * @since 0.0.4
     */
    public TextField getMaxCordField1() {
        return maxCordField1;
    }

    /**
     *
     * @return the minCordField2
     * @since 0.0.4
     */
    public TextField getMinCordField2() {
        return minCordField2;
    }

    /**
     *
     * @return the maxCordField2
     * @since 0.0.4
     */
    public TextField getMaxCordField2() {
        return maxCordField2;
    }

    /**
     *
     * @return the stepLabel
     * @since 0.0.4
     */
    public Label getStepLabel() {
        return stepLabel;
    }

    /**
     *
     * @return the logLabel2
     * @since 0.0.4
     */
    public Label getLogLabel2() {
        return logLabel2;
    }

    /**
     *  Calls the method to switch between the parameter and creation box.
     *  This method is called when the switch button is clicked.
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void switchBox(ActionEvent actionEvent) {
        swapHBox();
    }

    /**
     * Switches between the parameter and creation box.
     * If the parameter box is visible, it will set the parameter box to invisible and the creation box to visible.
     * If the creation box is visible, it will set the creation box to invisible and the parameter box to visible.
     *
     * @since 0.0.4
     */
    public void swapHBox() {
        getParameterBox().setVisible(!getParameterBox().isVisible());
        getCreationBox().setVisible(!getCreationBox().isVisible());
    }


    /**
     *
     * @return the stepField
     * @since 0.0.4
     */
    public TextField getStepField() {
        return stepField;
    }

    /**
     * Increases the min cords x0 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the min cords are more than the max cords, it will set the logLabel to "Min cords cannot be more than max cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void increaseMinCordX0Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMinCords = new Vector2D(chaosGameDescription.getMinCoords().getX0() + 1, chaosGameDescription.getMinCoords().getX1());
            chaosGameDescription.setMinCoords(newMinCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));

        }
    }

    /**
     * Decreases the min cords x0 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the min cords are more than the max cords, it will set the logLabel to "Min cords cannot be more than max cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void decreaseMinCordX0Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMinCords = new Vector2D(chaosGameDescription.getMinCoords().getX0() - 1, chaosGameDescription.getMinCoords().getX1());
            chaosGameDescription.setMinCoords(newMinCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException nullPointerException) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
        catch (IllegalArgumentException illegalArgumentException){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Increases the min cords x1 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the min cords are more than the max cords, it will set the logLabel to "Min cords cannot be more than max cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void decreaseMinCordX1Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMinCords = new Vector2D(chaosGameDescription.getMinCoords().getX0(), chaosGameDescription.getMinCoords().getX1() - 1);
            chaosGameDescription.setMinCoords(newMinCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue());
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Increases the min cords x1 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the min cords are more than the max cords, it will set the logLabel to "Min cords cannot be more than max cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void increaseMinCordX1Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMinCords = new Vector2D(chaosGameDescription.getMinCoords().getX0(), chaosGameDescription.getMinCoords().getX1() + 1);
            chaosGameDescription.setMinCoords(newMinCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(MessageRetrieval.getString(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue()));
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Decreases the max cords x0 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the max cords are less than the min cords, it will set the logLabel to "Max cords cannot be less than min cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void decreaseMaxCordX0Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMaxCords = new Vector2D(chaosGameDescription.getMaxCoords().getX0() - 1, chaosGameDescription.getMaxCoords().getX1());
            chaosGameDescription.setMaxCoords(newMaxCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue());
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Decreases the max cords x1 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the max cords are less than the min cords, it will set the logLabel to "Max cords cannot be less than min cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.47
     */
    public void increaseMaxCordX0Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMaxCords = new Vector2D(chaosGameDescription.getMaxCoords().getX0() + 1, chaosGameDescription.getMaxCoords().getX1());
            chaosGameDescription.setMaxCoords(newMaxCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue());
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Increases the max cords x1 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the max cords are less than the min cords, it will set the logLabel to "Max cords cannot be less than min cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void decreaseMaxCordX1Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMaxCords = new Vector2D(chaosGameDescription.getMaxCoords().getX0(), chaosGameDescription.getMaxCoords().getX1() - 1);
            chaosGameDescription.setMaxCoords(newMaxCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue());
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Increases the max cords x1 by 1.
     * It then updates the labels and restarts the chaos game with the new ChaosGameDescription.
     * It then updates the labels and draws the canvas.
     * If the ChaosGameDescription is null, it will set the logLabel to "Please choose a transformation".
     * If the max cords are less than the min cords, it will set the logLabel to "Max cords cannot be less than min cords".
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void increaseMaxCordX1Clicked(ActionEvent actionEvent) {
        try {
            ChaosGameDescription chaosGameDescription = getChaosGameHandler().getChaosGameDescription();
            Vector2D newMaxCords = new Vector2D(chaosGameDescription.getMaxCoords().getX0(), chaosGameDescription.getMaxCoords().getX1() + 1);
            chaosGameDescription.setMaxCoords(newMaxCords);
            updateLabels();
            restartChaosGame(false);
        } catch (NullPointerException e) {
            getLogLabel().setText(Messages.NO_TRANSFORMATION_SELECTED_WARNING.getValue());
        }
        catch (IllegalArgumentException e){
            getLogLabel().setText(MessageRetrieval.getString(Messages.INVALID_COORDS_WARNING.getValue()));
        }
    }

    /**
     * Sets up a new scene and loads the HomeView.
     * This method is called when the home button is clicked.
     *
     * @param actionEvent the action event that is triggered
     * @since 0.0.4
     */
    public void goHome(ActionEvent actionEvent) {
        Scene scene = ((Node) actionEvent.getSource()).getScene();
        scene.setRoot(FxApplication.loadFXML("HomeView"));
    }

    /**
     *
     * @return the parameterBox
     */
    public VBox getParameterBox() {
        return parameterBox;
    }

    /**
     *
     * @return the creationBox
     */
    public VBox getCreationBox() {
        return creationBox;
    }

    /**
     * Method to populate the fractal list.
     * This is an empty method as it is meant to be overridden by the classes that inherit this class.
     *
     * @since 0.0.5
     */
    public void populateFractalList(){
    }

    /**
     * @return the fractalList
     */
    public ListView<String> getFractalList() {
        return fractalList;
    }

    /**
     * Sets the event handler for the fractal list.
     * If the fractal list is not null, it will trigger an event when the user double clicks on a fractal.
     * It will get the name of the element clicked and call the loadFractalData method with the name as a parameter.
     *
     * @since 0.0.5
     */
    private void setEventHandler() {
        if(fractalList != null) {
            fractalList.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2) {
                    String fractalName = fractalList.getSelectionModel().getSelectedItem();
                    if(fractalName != null) {
                        loadFractalData(fractalName);
                    }
                }
            });
        }
    }

    /**
     * Loads the fractal data.
     * This is an empty method as it is meant to be overridden by the classes that inherit this class.
     *
     * @param fractalName the name of the fractal
     * @since 0.0.5
     */
    public void loadFractalData(String fractalName) {
    }

}