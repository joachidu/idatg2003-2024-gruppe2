package no.ntnu.exceptions;

/**
 * The {@code InvalidSaveStateException} class is a class that represents an exception that is thrown when the save state is invalid.
 *
 * @since 0.0.4
 * @version 0.0.4
 * @author Rolf Normann Flatøy
 */
public class InvalidSaveStateException extends RuntimeException {

    public InvalidSaveStateException(String message) {
        super(message);
    }

}
