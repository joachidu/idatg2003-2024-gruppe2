package no.ntnu.filemanipulation;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.JuliaTransform;

/**
 * The {@code SaveStateConverter} class is a class that contains methods for converting SaveState variables.
 * It can be used to compress the canvas array, or to convert lists of transformations in the generic 
 * type of double[] to lists of JuliaTransform or AffineTransform2D. 
 * 
 * @since 0.0.4
 * @version 0.0.4
 * @author Rolf Normann Flatøy
 */
public class SaveStateConverter {
    
    private SaveStateConverter() {
        
    }

    /**
     * This method stores the coordinates of where all the 1 values are, ignoring all 0 values.
     * The corresponding method decompressArray will return the array to its original format 
     * given the height and width of the original array.
     * 
     * @since 0.0.4
     */
    public static int[][] compressArray(int[][] canvas) {
        ArrayList<int[]> canvasCoordinates = new ArrayList<>();
        for (int i = 0; i < canvas.length; i++) {
            for (int j = 0; j < canvas[0].length; j++) {
                if (canvas[i][j] != 0) {
                    canvasCoordinates.add(new int[]{i,j});
                }
            }
        }
        return canvasCoordinates.toArray(new int[][]{});
    }

    /**
     * Does the reverse of compressArray. Goes from coordinates indicating the location of points on a canvas 
     * to the whole canvas containing ones and zeroes.
     * @param array the array to decompress
     * @param height the height of the resulting array
     * @param width the width of the resulting array
     * @return the decompressed array
     * 
     * @since 0.0.4
     */
    public static int[][] decompressArray(int[][] array, int height, int width) {
        int[][] canvas = new int[height][width];
        for (int[] point : array) {
            canvas[point[0]][point[1]] = 1;
        }
        return canvas;
    }

    /**
     * Converts a list of Juliatransforms to the general format of a list of double[]
     * @param transforms the list of julia transforms to convert
     * @return the converted list in double[] format
     * 
     * @since 0.0.4
     */
    public static List<double[]> juliaTransformToArray(List<JuliaTransform> transforms) {
        ArrayList<double[]> transformations = new ArrayList<>();
        for (JuliaTransform juliaTransform : transforms) {
            transformations.add(new double[]{juliaTransform.getPoint().getX0(), juliaTransform.getPoint().getX1()});
        }
        return transformations;
    }

    /**
     * Converts a list of AffineTransform2D to the general format of a list of double[]
     * @param transforms the list of affine transforms to convert
     * @return the converted list in double[] format
     * 
     * @since 0.0.4
     */
    public static List<double[]> affineTransformToArray(List<AffineTransform2D> transforms) {
        ArrayList<double[]> transformations = new ArrayList<>();
        for (AffineTransform2D affineTransform : transforms) {
            transformations.add(new double[] {
                affineTransform.getMatrix().getMatrix()[0][0], affineTransform.getMatrix().getMatrix()[0][1], 
                affineTransform.getMatrix().getMatrix()[1][0],affineTransform.getMatrix().getMatrix()[1][1], 
                affineTransform.getVector().getX0(), affineTransform.getVector().getX1()});
        }
        return transformations;
    }
}
