package no.ntnu.filemanipulation;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import no.ntnu.Main;
import no.ntnu.transformations.Transformation;

/**
 * This class is used to read a file containing the chaos game description.
 * The file contains the name of the transformation, the lower and upper corner of the fractal,
 * and a list of transformations.
 *
 * @author Rolf Normann Flatøy
 * @version 0.0.2
 * @since 0.0.2
 */
public class ChaosGameFileHandler {
    public static final String JULIA_TRANSFORM = "Data/Julia.txt";
    public static final String BARNSLEY_FERN_TRANSFORM = "Data/Barnsley-fern.txt";
    public static final String SIERPINSKI_TRANSFORM = "Data/Sierpinski.txt";
    
    /**
     * Creates and sets up the transformation given the filepath. 
     * Effectively the same as creating an instance of this class and running 
     * setUpTransformation.
     * @param filePath
     * 
     * @since 0.0.2
     */
    public ChaosGameFileHandler(String filePath) {
        getTransformation(filePath);
    }

    /**
     * Creates an uninitialized ChaosGameFileHandler. 
     * setUpTransformation needs to be called to get a transformation. 
     */
    public ChaosGameFileHandler() {

    }

    /**
     * Sets up the transformation according to the file in the filepath. 
     * It can then be accessed through any of the get methods.  
     * @param resourcesPath path to the resources file, the transformation to set up
     * @since 0.0.2
     */
    public Transformation getTransformation(String resourcesPath) {
        Transformation transformation = new Transformation();
        try {
            Scanner scanner = new Scanner(getFile(resourcesPath));
            scanner.useLocale(Locale.ENGLISH); // Use a dot as decimal separator (not a comma
            scanner.useDelimiter(",\\s|" + System.lineSeparator()); // Using both comma and newline as delimiters

            setTransformationType(scanner.nextLine(), transformation);
            setLowerCorner(new double[]{scanner.nextDouble(), scanner.nextDouble()}, transformation);
            setUpperCorner(new double[]{scanner.nextDouble(), scanner.nextDouble()}, transformation);

            ArrayList<double[]> tempArray = new ArrayList<>();
            while (scanner.hasNextLine()) {
                double[] temp = new double[6];
                for (int i = 0; i < 6 && scanner.hasNextDouble(); i++) {
                    temp[i] = scanner.nextDouble();
                }
                tempArray.add(temp);
            }
            setTransformations(tempArray, transformation);
            scanner.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transformation;
    }


    /**
     * Gets the file from the given path
     *
     * @param path the path to the file
     * @return the file as an InputStream
     * @since 0.0.2
     */
    private InputStream getFile(String path) {
        ClassLoader classLoader = Main.class.getClassLoader();
        return classLoader.getResourceAsStream(path);
    }

    /**
     * Sets the transformation name
     * @param name the name of the transformation, it can be either "Affine2D" or "Julia"
     * @since 0.0.2
     */
    private void setTransformationType(String name, Transformation transformation) {
        if(name == null) {
            throw new IllegalArgumentException("Transformation name cannot be null");
        }
        transformation.setTransformationType(name);
    }

    /**
     * Sets the lower corner of the fractal
     * @param lowerCorner the lower corner of the fractal
     * @since 0.0.2
     */
    private void setLowerCorner(double[] lowerCorner, Transformation transformation) {
        if(lowerCorner == null) {
            throw new IllegalArgumentException("Lower corner cannot be null");
        }
        transformation.setLowerCorner(lowerCorner);
    }

    /**
     * Sets the upper corner of the fractal
     * @param upperCorner the upper corner of the fractal
     * @since 0.0.2
     */
    private void setUpperCorner(double[] upperCorner, Transformation transformation) {
        if(upperCorner == null) {
            throw new IllegalArgumentException("Upper corner cannot be null");
        }
        transformation.setUpperCorner(upperCorner);
    }

    /**
     * @param transformations the transformations to set, cannot be null
    * @throws IllegalArgumentException if the specified transformations is null
     * @since 0.0.2
     */
    private void setTransformations(ArrayList<double[]> transformations, Transformation transformation) {
        if(transformations == null) {
            throw new IllegalArgumentException("Transformations cannot be null");
        }
        transformation.setTransformations(transformations);
    }
}
