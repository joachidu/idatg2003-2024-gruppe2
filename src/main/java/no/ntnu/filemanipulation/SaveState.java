package no.ntnu.filemanipulation;

import java.util.List;
import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.JuliaTransform;
import no.ntnu.unitclasses.Vector2D;

/**
 * The {@code SaveState} class is a class that has contains all the necessary data for 
 * preserving the state of the program. 
 * 
 * @since 0.0.4
 * @version 0.0.4
 * @author Rolf Normann Flatøy
 */
public class SaveState {
    private int[][] canvas;
    private int height;
    private int width;
    private Vector2D minCoords;    
    private Vector2D maxCoords;    
    private List<AffineTransform2D> affineTransforms;
    private List<JuliaTransform> juliaTransforms;
    private int steps;
    private String transformationType;
    private double windowWidth;
    private double windowHeight;
    private List<Vector2D> allPoints;
    private List<Integer> transformOrder;
    private int reRunSteps;

    private SaveState(SaveStateBuilder builder) {
        this.canvas = builder.canvas;
        this.height = builder.height;
        this.width = builder.width;
        this.maxCoords = builder.maxCoords;
        this.minCoords = builder.minCoords;
        this.affineTransforms = builder.affineTransforms;
        this.juliaTransforms = builder.juliaTransforms;
        this.steps = builder.steps;
        this.transformationType = builder.transformationType;
        this.windowWidth = builder.windowWidth;
        this.windowHeight = builder.windowHeight;
        this.allPoints = builder.allPoints;
        this.transformOrder = builder.transformOrder;
        this.reRunSteps = builder.reRunSteps;
    }

    /**
     * Verifies that there are no null or negative values, as well
     * as a basic check to see that max coords is bigger than min coords.
     * @return true if the state contains no null or negative values, false otherwise.
     */
    public boolean verifySave() {
        if (minCoords == null || maxCoords == null) {
            return false;
        }
        
        if (minCoords.getX0() > maxCoords.getX0() || minCoords.getX1() > maxCoords.getX1()) {
            return false;
        }
        
        if (height < 0 || width < 0 || windowHeight < 0 || windowWidth < 0) {
            return false;
        }
        
        if ((affineTransforms == null && juliaTransforms == null) || transformationType == null) {
            return false;
        }
        
        if (canvas == null || steps < 0 || allPoints == null) {
            return false;
        }
        return true;
    }

    public int getReRunSteps() {
        return reRunSteps;
    }

    public void setReRunSteps(int reRunSteps) {
        this.reRunSteps = reRunSteps;
    }

    public List<Integer> getTransformOrder() {
        return transformOrder;
    }

    public void setTransformOrder(List<Integer> transformOrder) {
        this.transformOrder = transformOrder;
    }

    public List<Vector2D> getAllPoints() {
        return allPoints;
    }

    public double getWindowHeight() {
        return windowHeight;
    }

    public void setWindowHeight(double windowHeight) {
        this.windowHeight = windowHeight;
    }

    public double getWindowWidth() {
        return windowWidth;
    }

    public void setWindowWidth(double windowWidth) {
        this.windowWidth = windowWidth;
    }

    public String getTransformationType() {
        return transformationType;
    }

    public void setTransformType(String transformationType) {
        this.transformationType = transformationType;
    }

    public int getSteps() {
        return steps;
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public int[][] getCanvas() {
        return canvas;
    }

    public void setCanvas(int[][] canvas) {
        this.canvas = canvas;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Vector2D getMinCoords() {
        return minCoords;
    }

    public void setMinCoords(Vector2D minHeight) {
        this.minCoords = minHeight;
    }

    public Vector2D getMaxCoords() {
        return maxCoords;
    }

    public void setMaxCoords(Vector2D maxHeight) {
        this.maxCoords = maxHeight;
    }

    public List<AffineTransform2D> getAffineTransforms() {
        return affineTransforms;
    }

    public void setAffineTransforms(List<AffineTransform2D> transforms) {
        this.affineTransforms = transforms;
    }

    public List<JuliaTransform> getJuliaTransforms() {
        return juliaTransforms;
    }

    public void setJuliaTransforms(List<JuliaTransform> juliaTransforms) {
        this.juliaTransforms = juliaTransforms;
    }

    public static class SaveStateBuilder {
        private int[][] canvas;
        private int height;
        private int width;
        private Vector2D minCoords;    
        private Vector2D maxCoords;    
        private List<AffineTransform2D> affineTransforms;
        private List<JuliaTransform> juliaTransforms;
        private int steps;
        private String transformationType;
        private double windowWidth;
        private double windowHeight;
        private List<Vector2D> allPoints;
        private List<Integer> transformOrder;
        private int reRunSteps;

        public SaveStateBuilder reRunSteps(int reRunSteps) {
            this.reRunSteps = reRunSteps;
            return this;
        }

        public SaveStateBuilder transformOrder(List<Integer> transformOrder) {
            this.transformOrder = transformOrder;
            return this;
        }

        public SaveStateBuilder allPoints(List<Vector2D> allPoints) {
            this.allPoints = allPoints;
            return this;
        }

        public SaveStateBuilder windowWidth(double windowWidth) {
            this.windowWidth = windowWidth;
            return this;
        }

        public SaveStateBuilder windowHeight(double windowHeight) {
            this.windowHeight = windowHeight;
            return this;
        }

        public SaveStateBuilder transformationType(String transformationType) {
            this.transformationType = transformationType;
            return this;
        }

        public SaveStateBuilder canvas(int[][] canvas) {
            this.canvas = canvas;
            return this;
        }

        public SaveStateBuilder height(int height) {
            this.height = height;
            return this;
        }

        public SaveStateBuilder width(int width) {
            this.width = width;
            return this;
        }

        public SaveStateBuilder minCoords(Vector2D minCoords) {
            this.minCoords = minCoords;
            return this;
        }

        public SaveStateBuilder maxCoords(Vector2D maxCoords) {
            this.maxCoords = maxCoords;
            return this;
        }

        public SaveStateBuilder affineTransforms(List<AffineTransform2D> affineTransforms) {
            this.affineTransforms = affineTransforms;
            return this;
        }

        public SaveStateBuilder juliaTransforms(List<JuliaTransform> juliaTransforms) {
            this.juliaTransforms = juliaTransforms;
            return this;
        }

        public SaveStateBuilder steps(int steps) {
            this.steps = steps;
            return this;
        }

        public SaveState build() {
            return new SaveState(this);
        }
    }
}
