package no.ntnu.filemanipulation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import no.ntnu.chaosgame.ChaosGameHandler;
import no.ntnu.exceptions.InvalidSaveStateException;
import no.ntnu.transformations.Transformation;

/**
 * The SaveManager class is responsible for saving and loading the state of the program, as well as saving and loading fractals. 
 * 
 * @author Rolf Normann Flatøy
 * @version 0.0.4
 * @since 0.0.4
 */
public class SaveManager {

    private static final String SAVE_DIRECTORY = System.getProperty("user.home") + "\\.fractorial\\";
    private static final String SAVESTATE_FILE = SAVE_DIRECTORY + "savestate.txt";
    private static final String FRACTALS_DIRECTORY = SAVE_DIRECTORY + "\\fractals\\";
    private static SaveManager instance;
    private static final Logger logger = LogManager.getLogger(SaveManager.class);

    /*
     * A private Constructor to prevent object creation outside of the getInstance method.
     */
    private SaveManager() {

    }

    public static SaveManager getInstance() {
        if (instance == null) {
            instance = new SaveManager();
        }
        return instance;
    }

    private void createFractorialDirectory() {
        File f = new File(SAVE_DIRECTORY);
        if (!f.exists()) {
            f.mkdir();
        }
    }

    /**
     * Saves the state of the program to the disk. 
     * @param state the state to save.
     * @throws IOException if the state could not be saved. 
     */
    public void saveSaveState(SaveState state) throws IOException {   
        createFractorialDirectory();
        logger.info("Saving state...");
        File f = new File(Path.of(SAVESTATE_FILE).getParent().toString());
        if (!f.exists()) {
            f.mkdir();
        }
        state.setCanvas(SaveStateConverter.compressArray(state.getCanvas()));
        if (state.verifySave()) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(state);
            try(FileOutputStream ofs = new FileOutputStream(SAVESTATE_FILE, false)) {
                ofs.write(jsonString.getBytes(StandardCharsets.UTF_8));
            } catch (IOException ex) {
                logger.error("Failed to save the state", ex);
                throw ex;
            }
        } else {
            InvalidSaveStateException invalidSaveStateException = new InvalidSaveStateException(
                "Failed to save SaveState: the SaveState.verifySave method returned false");
            logger.warn("Tried to save an invalid savestate.", invalidSaveStateException);
            throw invalidSaveStateException;
        }
    }

    /**
     * Gets the type of transformation the user was doing before they closed the program.
     * @return either Julia or Affine2D.
     * 
     * @since 0.0.3
     */
    public String getTransformationType() throws IOException, JsonSyntaxException {
        SaveState state = loadSaveState();
        if (state != null) {
            return state.getTransformationType();
        }
        else return null;
    }

    /**
     * Loads the previous savestate, returns null if there is no savestate saved. 
     * @return the savestate the user previously used, or null if there isn't one.
     * 
     * @since 0.0.3
     */
    public SaveState loadSaveState() throws IOException, JsonSyntaxException {
        String saveStateString = null;
        File f = new File(SAVESTATE_FILE);
        if (!f.exists()) {
            return null;
        }
        if (f.isDirectory()) {
            throw new FileNotFoundException("Unable to load the savestate file: The savestate file is a directory.");
        }
        try (FileInputStream stream = new FileInputStream(SaveManager.SAVESTATE_FILE)) {
            saveStateString = new String(stream.readAllBytes());
        } catch (IOException ioException) {
            logger.error("Failed loading savestate.", ioException);
            throw ioException;
        }
        Gson gson = new Gson();
        SaveState state;
        state = gson.fromJson(saveStateString, SaveState.class);

        if (state == null) {
            return null; // There was an empty save file
        }
        state.setCanvas(SaveStateConverter.decompressArray(state.getCanvas(), state.getHeight(), state.getWidth()));
        return state;
    }

    /**
     * Saves a fractal to the disk.
     * @param fractal the fractal to save.
     */
    public void saveFractal(Transformation fractal) {
        createFractorialDirectory();
        Gson gson = new Gson();
        File f = new File(Path.of(FRACTALS_DIRECTORY).toString());
        if (!f.exists()) {
            f.mkdir();
        }
        String fractalName;
        if (fractal.getTransformationType().equals(ChaosGameHandler.AFFINE_TRANSFORM)) {
            fractalName = "a" + fractal.getTransformationName();
        } else {
            fractalName = "j" + fractal.getTransformationName();
        }
        try(FileOutputStream ofs = new FileOutputStream(FRACTALS_DIRECTORY + fractalName, false)) {
            ofs.write(gson.toJson(fractal).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the specified type of saved fractals from the disk. 
     * @param affineFractals true if affine fractals should be loaded, false if julia fractals should be loaded.
     * @return a list of paths to the saved fractals.
     */
    public List<Path> getFractalNamesByType(boolean affineFractals) {
        createFractorialDirectory();
        try {
            File f = new File(FRACTALS_DIRECTORY);
            if (!f.exists()) {
                f.mkdir();
            }
            if (!f.isDirectory()) {
                logger.error("Unable to load fractals: Fractals is a directory.");
                throw new UnsupportedOperationException("Fractals is a directory");
            }
            try (Stream<Path> paths = Files.list(Path.of(FRACTALS_DIRECTORY))) {
                return paths.filter(Files::isRegularFile)
                .filter(x -> (x.getFileName().toString().startsWith("a") && affineFractals) || 
                    (x.getFileName().toString().startsWith("j") && !affineFractals))
                .toList();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    /**
     * Loads a fractal from the disk.
     * @param fractalName the name of the fractal to load.
     * @return the fractal that was loaded.
     */
    public Transformation loadFractal(String fractalName) {
        String saveStateString = null;
        try (FileInputStream stream = new FileInputStream(FRACTALS_DIRECTORY + fractalName)) {
            saveStateString = new String(stream.readAllBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (saveStateString != null) {
            Gson gson = new Gson();
            return gson.fromJson(saveStateString, Transformation.class);
        }
        return null;
    }

    /**
     * Gets the names of all saved fractals.
     * @param fractalName the name of the fractal to delete.
     */
    public List<String> getFractalNames() {
        List<String> fractalNames = new ArrayList<>();
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(FRACTALS_DIRECTORY))) {
            for (Path entry : stream) {
                fractalNames.add(entry.getFileName().toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
    }
    return fractalNames;
    }
}

