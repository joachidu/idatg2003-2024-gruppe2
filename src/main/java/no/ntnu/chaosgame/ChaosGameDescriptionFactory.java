package no.ntnu.chaosgame;

import no.ntnu.filemanipulation.ChaosGameFileHandler;
import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.JuliaTransform;
import no.ntnu.transformations.Transformation;
import no.ntnu.unitclasses.Complex;
import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.transformations.Transform2D;
import no.ntnu.unitclasses.Vector2D;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code ChaosGameDescriptionFactory} is a class that creates a {@code ChaosGameDescription} from a file
 * or a custom input.
 *
 *
 * @author Joachim Duong
 * @version 0.0.3
 * @since 0.0.3
 */
public class ChaosGameDescriptionFactory {
    ArrayList<double[]> transformations;
    Vector2D minHeight;
    Vector2D maxHeight;
    ChaosGameFileHandler chaosGameFileHandler;
    public static final String AFFINE = "Affine2D";

    /**
     * Creates a {@code ChaosGameDescription} from a file with the specified name.
     * It reads a file from the data folder and creates a {@code ChaosGameDescription} from the data in the file.
     * It sets the fields of the class to the values read from the file.
     * It thens calls a method to create the {@code ChaosGameDescription} and returns it, based on the type of transformation.
     *
     * @param name the name of the file to read from
     * @return a {@code ChaosGameDescription} created from the file
     * @since 0.0.3
     */
    public ChaosGameDescription getDescriptionFromFile(String name){
        if (chaosGameFileHandler == null) {
            chaosGameFileHandler = new ChaosGameFileHandler();
        }
        String filePath = "Data/" + name + ".txt";
        Transformation transformation = chaosGameFileHandler.getTransformation(filePath);
        double[] lowerCorner = transformation.getLowerCorner();
        double[] upperCorner = transformation.getUpperCorner();
        transformations = new ArrayList<>(transformation.getTransformations());
        minHeight = new Vector2D(lowerCorner[0], lowerCorner[1]);
        maxHeight = new Vector2D(upperCorner[0], upperCorner[1]);

        ChaosGameDescription chaosGameDescription;
        if(transformation.getTransformationType().equals(AFFINE)) {
            chaosGameDescription = createDescriptionAffine2D();
        }
        else {
            chaosGameDescription = createDescriptionJulia();
        }
        return chaosGameDescription;
    }

    /**
     * Creates a {@code ChaosGameDescription} from a custom input. This is used when the List
     * is of the general type {@code double[]}.
     * It takes in minimum and maximum height, a list of transformations and a boolean to determine
     * if the transformation is affine or not.
     * It then calls the method to create the {@code ChaosGameDescription} and returns it.
     *
     * @param minHeight the minimum height of the canvas
     * @param maxHeight the maximum height of the canvas
     * @param transformations the list of transformations
     * @param isAffine a boolean to determine if the transformation is affine or not
     * @return a {@code ChaosGameDescription} created from the custom input
     * @since 0.0.3
     */
    public ChaosGameDescription getCustomDescription(Vector2D minHeight, Vector2D maxHeight, List<double[]> transformations, boolean isAffine) {
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        this.transformations = new ArrayList<>(transformations);
        ChaosGameDescription chaosGameDescription;
        if(isAffine) {
            chaosGameDescription = createDescriptionAffine2D();
        }
        else {
            chaosGameDescription = createDescriptionJulia();
        }
        return chaosGameDescription;
    }

    /**
     * Creates a {@code ChaosGameDescription} from a custom input. This is used when the List is
     * of type {@code AffineTransform2D}.
     * It takes in minimum and maximum height, a list of transformations and a boolean to determine
     * if the transformation is affine or not.
     * It then calls the method to create the {@code ChaosGameDescription} and returns it.
     *
     * @param minHeight the minimum height of the canvas
     * @param maxHeight the maximum height of the canvas
     * @param transformations
     * @return a {@code ChaosGameDescription} created from the custom input
     * @since 0.0.3
     */
    public ChaosGameDescription getCustomDescriptionAffine(Vector2D minHeight, Vector2D maxHeight, List<AffineTransform2D> transformations) {
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        List<Transform2D> createdTransformations = new ArrayList<>(transformations);
        return new ChaosGameDescription(minHeight, maxHeight, createdTransformations, AFFINE);
    }

    /**
     * Creates a {@code ChaosGameDescription} from a custom input. This is used when the List is
     * of type {@code JuliaTransform}.
     * It takes in minimum and maximum height, a list of transformations and a boolean to determine
     * if the transformation is affine or not.
     * It then calls the method to create the {@code ChaosGameDescription} and returns it.
     *
     * @param minHeight the minimum height of the canvas
     * @param maxHeight the maximum height of the canvas
     * @param transformations the list of transformations
     * @return a {@code ChaosGameDescription} created from the custom input
     * @since 0.0.3
     */
    public ChaosGameDescription getCustomDescriptionJulia(Vector2D minHeight, Vector2D maxHeight, List<JuliaTransform> transformations) {
        this.minHeight = minHeight;
        this.maxHeight = maxHeight;
        List<Transform2D> createdTransformations = new ArrayList<>(transformations);
        return new ChaosGameDescription(minHeight, maxHeight, createdTransformations, "Julia");
    }


    /**
     * Creates a {@code ChaosGameDescription} from the fields of the class.
     * It creates a list of {@code Transform2D} from the list of transformations.
     * This is used when the transformation is affine.
     *
     * @return a {@code ChaosGameDescription} created from the fields of the class
     * @since 0.0.3
     */
    private ChaosGameDescription createDescriptionAffine2D() {
        ArrayList<Transform2D> createdTransformation = new ArrayList<>();
        for (double[] transformation : transformations) {
            Matrix2D matrix = new Matrix2D(transformation[0], transformation[1], transformation[2], transformation[3]);
            Vector2D vector = new Vector2D(transformation[4], transformation[5]);
            createdTransformation.add(new AffineTransform2D(matrix, vector));
        }
        return new ChaosGameDescription(minHeight, maxHeight, createdTransformation, AFFINE);
    }

    /**
     * Creates a {@code ChaosGameDescription} from the fields of the class.
     * It creates a list of {@code Transform2D} from the list of transformations.
     * This is used when the transformation is not affine.
     *
     * @return a {@code ChaosGameDescription} created from the fields of the class
     * @since 0.0.3
     */
    private ChaosGameDescription createDescriptionJulia() {
        ArrayList<Transform2D> createdTransformation = new ArrayList<>();
        Complex c = new Complex(transformations.get(0)[0], transformations.get(0)[1]);
        JuliaTransform juliaTransform = new JuliaTransform(c, 1);
        createdTransformation.add(juliaTransform);
        JuliaTransform juliaTransform2 = new JuliaTransform(c, -1);
        createdTransformation.add(juliaTransform2);
        return new ChaosGameDescription(minHeight, maxHeight, createdTransformation, "Julia");
    }
}
