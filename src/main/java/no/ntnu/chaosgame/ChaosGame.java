package no.ntnu.chaosgame;

import java.util.Random;
import no.ntnu.transformations.Transform2D;
import no.ntnu.unitclasses.Vector2D;
import java.util.ArrayList;
import java.util.List;
/**
 * This class is used to calculate the fractals of the chaos game.
 *
 * It contains the minimum and maximum coordinates of the fractal, and a list of transforms.
 * With this information, the fractal can be calculated and limited to the canvas.
 *
 * @author Joachim Duong
 * @version 0.0.2
 * @since 0.0.2
 */
public class ChaosGame {
    private ChaosGameDescription chaosGameDescription;
    private ChaosGameCanvas chaosGameCanvas;
    private Vector2D currentPoint;
    private Random random;
    private ArrayList<Integer> transformOrder;
    private int reRunSteps;

    /**
     * Constructor for the ChaosGame class
     * @param chaosGameDescription the chaos game description to set
     * @since 0.0.2
     */
    public ChaosGame(ChaosGameDescription chaosGameDescription) {
        setChaosGameDescription(chaosGameDescription);
        setCurrentPoint(new Vector2D(0, 0));
        random = new Random();
    }

    /**
     * Creates a new chaosgame with the specified description and specified point. 
     * @param chaosGameDescription the description to set. 
     * @param currentPoint the point to set.
     */
    public ChaosGame(ChaosGameDescription chaosGameDescription, Vector2D currentPoint) {
        setChaosGameDescription(chaosGameDescription);
        setCurrentPoint(currentPoint);
        random = new Random();
    }

    public List<Integer> getTransformOrder() {
        return transformOrder;
    }

    public int getReRunSteps() {
        return reRunSteps;
    }

    /**
     * Runs the chaos game for a given amount of steps and puts the pixels on the canvas.
     * It randomly selects one of the transforms and applies it to the current point.
     * It then puts the pixel on the canvas.
     * Then it sets the new point as current point for the next iteration.
     *
     * @param steps the amount of steps to run
     * @since 0.0.2
     */
    public void runSteps(int steps) {
        transformOrder = new ArrayList<>();
        reRunSteps = steps;
        for (int i = 0; i < steps; i++) {
            int index = random.nextInt(chaosGameDescription.getTransforms().size());
            transformOrder.add(index);
            Transform2D transformation = chaosGameDescription.getTransforms()
                .get(index);
            Vector2D newPoint = transformation.transform(currentPoint);
            chaosGameCanvas.putPixel(newPoint);
            currentPoint = newPoint;
        }
    }

    public void generatePreviousFractal() {
        currentPoint = new Vector2D(0,0);
        for (int i = 0; i < reRunSteps; i++) {
            int index = transformOrder.get(i);
            Transform2D transformation = chaosGameDescription.getTransforms()
                .get(index);
            Vector2D newPoint = transformation.transform(currentPoint);
            chaosGameCanvas.putPixel(newPoint);
            currentPoint = newPoint;
        }
    }

    public void setTransformOrder(List<Integer> transformOrder) {
        this.transformOrder = (ArrayList<Integer>)transformOrder;
    }

    public void setReRunSteps(int reRunSteps) {
        this.reRunSteps = reRunSteps;
    }

    /**
     * sets the chaos game description
     * @param chaosGameDescription the chaos game description to set, cannot be null
     * @since 0.0.2
     */
    public void setChaosGameDescription(ChaosGameDescription chaosGameDescription) {
        if (chaosGameDescription == null) {
            throw new IllegalArgumentException("ChaosGameDescription cannot be null");
        }
        this.chaosGameDescription = chaosGameDescription;
    }

    /**
     *
     * @param currentPoint the current point to set, cannot be null
     * @since 0.0.2
     */
    public void setCurrentPoint(Vector2D currentPoint) {
        if (currentPoint == null) {
            throw new IllegalArgumentException("CurrentPoint cannot be null");
        }
        this.currentPoint = currentPoint;
    }

    /**
     * sets the chaos game canvas to the specified chaos game canvas
     * @param chaosGameCanvas the chaos game canvas to set, cannot be null
     * @since 0.0.2
     */
    public void setChaosGameCanvas(ChaosGameCanvas chaosGameCanvas) {
        if (chaosGameCanvas == null) {
            throw new IllegalArgumentException("ChaosGameCanvas cannot be null");
        }
        this.chaosGameCanvas = chaosGameCanvas;
    }


}
