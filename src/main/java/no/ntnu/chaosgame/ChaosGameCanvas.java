package no.ntnu.chaosgame;

import java.util.List;
import no.ntnu.trackers.DuplicateTracker;
import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.unitclasses.Matrix2D;
import no.ntnu.unitclasses.Vector2D;

/**
 * The {@code ChaosGameCanvas} class is a class that represents a canvas for the ChaosGame.
 * It has a width, height, minCoords, maxCoords and an underlying array representing the canvas.
 *
 * @author Rolf Normann Flatøy
 * @version 0.0.1
 * @since 0.0.1
 */
public class ChaosGameCanvas {
    private int[][] canvas;
    private int width;
    private int height;
    private Vector2D minCoords;
    private Vector2D maxCoords;
    private DuplicateTracker<Vector2D> points;

    /**
     * Creates a new instance of ChaosGameCanvas with the specified values and an underlying array of zeroes. 
     * @param width the width to set. 
     * @param height the height to set. 
     * @param minCoords the minCoords to set. 
     * @param maxCoords the maxCoords to set
     * 
     * @since 0.0.1
     */
    public ChaosGameCanvas (int width, int height, Vector2D minCoords, Vector2D maxCoords) {
        points = new DuplicateTracker<>();
        setWidth(width);
        setHeight(height);
        setMaxCoords(maxCoords);
        setMinCoords(minCoords);
        clearCanvas();
    }

    public void setPoints(List<Vector2D> points) {
        this.points = new DuplicateTracker<>();
        for (Vector2D vector : points) {
            this.points.addObject(vector);
        }
    }

    public List<Vector2D> getAllPoints() {
        return points.getAllPoints();
    }

    public int getOccurances(Vector2D point) {
        return points.getOccurances(point);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    /**
     * Clears the canvas
     * @since 0.0.1
     */
    public void clearCanvas() {
        canvas = new int[height][width];
    }

    /**
     * Sets the max coords
     *
     * @throws IllegalArgumentException if max coords is less than min coords
     * @param maxCoords the max coords
     * @since 0.0.1
     */
    public void setMaxCoords(Vector2D maxCoords) {
        if (minCoords != null && maxCoords != null && (
            maxCoords.getX0() < minCoords.getX0() ||
            maxCoords.getX1() < minCoords.getX1())) {
            throw new IllegalArgumentException("The max coords cannot be less than the min coords and cannot be null");
        }
        this.maxCoords = maxCoords;
    }

    private void setWidth(int width) {
        this.width = width;
    }

    private void setHeight(int height) {
        this.height = height;
    }

    /**
     * Sets the min coords
     *
     * @throws IllegalArgumentException if max coords is less than min coords
     * @param minCoords the min coords
     * @since 0.0.1
     */
    private void setMinCoords(Vector2D minCoords) {
        if (maxCoords.getX0() < minCoords.getX0() ||
            maxCoords.getX1() < minCoords.getX1()) {
            throw new IllegalArgumentException("The min coords cannot be larger than the max coords and cannot be null");
        }

        this.minCoords = minCoords;
    }

    /**
     * Puts a pixel on the canvas
     * It transforms the coords to indices and puts the pixel on the canvas
     * if the coords are within the canvas
     *
     * @param point the point to put on the canvas
     * @since 0.0.1
     */
    public void putPixel(Vector2D point) {
        Vector2D indices = transformCoordsToIndices(point);
        int x = (int) indices.getX0();
        int y = (int) indices.getX1();
        if(point.getX0() >= minCoords.getX0() && point.getX0() <= maxCoords.getX0() &&
        point.getX1() >= minCoords.getX1() && point.getX1() <= maxCoords.getX1()){
            points.addObject((new Vector2D(x,y)));
            canvas[x][y] = 1;
        }
    }

    /**
     * Returns the value of the pixel at the given point. 
     * The point's coordinates is used to index the array to get the value. 
     * @param point the point to get the pixel from
     * @return the value of the pixel
     * 
     * since 0.0.2
     */
    public int getPixel(Vector2D point) {
        double x = point.getX0();
        double y = point.getX1();
        if (x < 0 || x >= width || y < 0 || y >= height) {
            return 0;
        }
        return canvas[(int) y][(int) x];
    }

    /**
     *
     * @return the max coords
     * @since 0.0.1
     */
    public Vector2D getMaxCoords() {
        return maxCoords;
    }

    /**
     *
     * @return the min coords
     * @since 0.0.1
     */
    public Vector2D getMinCoords() {
        return minCoords;
    }

    /**
     * Transforms the coords to indices
     * It
     * @param coords the coords to transform
     * @return the indices
     */
    private Vector2D transformCoordsToIndices(Vector2D coords) {
        AffineTransform2D transformCoordsToIndices = new AffineTransform2D(
            new Matrix2D(0,
                (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
                (width - 1) / (maxCoords.getX0() - minCoords.getX0()),
                0)
            , new Vector2D(
            ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
            ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0())));
        return transformCoordsToIndices.transform(coords);
    }

    /**
     * Returns the underlying canvas 
     * @return the canvas of the ChaosGame
     * 
     * since 0.0.1
     */
    public int[][] getCanvas() {
        return canvas;
    }

    public void setCanvas(int[][] canvas) {
        this.canvas = canvas;
    }
}