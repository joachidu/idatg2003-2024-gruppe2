package no.ntnu.chaosgame;

import java.util.List;
import no.ntnu.transformations.Transform2D;
import no.ntnu.unitclasses.Vector2D;
/**
 * This class is used to calculate the fractals of the chaos game.
 * It contains the minimum and maximum coordinates of the fractal, and a list of transforms.
 * With this information, the fractal can be calculated and limited to the canvas.
 * 
 * @version 0.0.2
 * @since 0.0.2
 * @author Joachim Duong
 */
public class ChaosGameDescription {
    private Vector2D minCoords;
    private Vector2D maxCoords;
    private List<Transform2D> transforms;
    private String transformType;

    /**
     * Constructor for the ChaosGameDescription
     * This is used for preset fractals.
     * @param minCoords the minimum coordinates of the canvas
     * @param maxCoords the maximum coordinates of the canvas
     * @param transforms the list of the transforms
     */
    public ChaosGameDescription(Vector2D minCoords, Vector2D maxCoords, List<Transform2D> transforms, String transformType) {
        setMinCoords(minCoords);
        setMaxCoords(maxCoords);
        setTransforms(transforms);
        this.transformType = transformType;
    }

    public String getTransformType() {
        return transformType;
    }

    public void setTransformType(String transformType) {
        this.transformType = transformType;
    }

    /**
     * Alternative constructor for the ChaosGameDescription
     * This is for an empty fractal.
     */
    public ChaosGameDescription() {
    }

    /**
     * Set the minimum coordinates of the canvas.
     * @param minCoords the minimum coordinates of the canvas
     */
    public void setMinCoords(Vector2D minCoords) {
        if (minCoords == null) {
            throw new IllegalArgumentException("MinCoords cannot be null");
        }
        else if(maxCoords != null && (maxCoords.getX0() <= minCoords.getX0() || maxCoords.getX1() <= minCoords.getX1())) {
                throw new IllegalArgumentException("MinCoords cannot be larger than MaxCoords");
            }

        this.minCoords = minCoords;
    }

    /**
     * Set the maximum coordinates of the canvas.
     * @param maxCoords the maximum coordinates of the canvas
     */
    public void setMaxCoords(Vector2D maxCoords) {
        if (maxCoords == null) {
            throw new IllegalArgumentException("MaxCoords cannot be null");
        }

        else if(minCoords != null && 
            (maxCoords.getX0() <= minCoords.getX0() || maxCoords.getX1() <= minCoords.getX1())) {
                throw new IllegalArgumentException("MaxCoords cannot be smaller than MinCoords");
        }
        this.maxCoords = maxCoords;
    }

    /**
     * Set the list of the transforms.
     * @param transforms is the list containing the transformations
     */
    public void setTransforms(List<Transform2D> transforms) {
        if (transforms == null) {
            throw new IllegalArgumentException("Transforms cannot be null");
        }
        this.transforms = transforms;
    }

    /**
     *
     * @return the minimum coordinates of the canvas
     */
    public Vector2D getMinCoords() {
        return minCoords;
    }

    /**
     *
     * @return the maximum coordinates of the canvas
     */
    public Vector2D getMaxCoords() {
        return maxCoords;
    }

    /**
     *
     * @return the list of the transforms
     */
    public List<Transform2D> getTransforms() {
        return transforms;
    }

}
