package no.ntnu.chaosgame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import no.ntnu.filemanipulation.SaveManager;
import no.ntnu.filemanipulation.SaveState;
import no.ntnu.filemanipulation.SaveStateConverter;
import no.ntnu.filemanipulation.SaveState.SaveStateBuilder;
import no.ntnu.transformations.AffineTransform2D;
import no.ntnu.transformations.JuliaTransform;
import no.ntnu.transformations.Transformation;
import no.ntnu.transformations.Transform2D;

/**
 * The {@code ChaosGameHandler} is a class that represent the handler for the chaos game.
 * It contains the canvas and the chaos game. It also contains the chaos game description.
 * Using the {@code ChaosGameDescription} it creates a {@code ChaosGame} with a specific set of transformations.
 * It then runs the chaos game with a specified number of steps.
 *
 * <blockquote><pre>
 *     ChaosGameHandler chaosGameHandler = new ChaosGameHandler();
 * </pre></blockquote>
 *
 *
 * @author Joachim Duong
 * @version 0.0.2
 * @since 0.0.2
 */

public class ChaosGameHandler {
    Scanner scanner = new Scanner(System.in);
    private ChaosGameCanvas chaosGameCanvas;
    private ChaosGame chaosGame;
    private ChaosGameDescription chaosGameDescription;
    private ChaosGameDescriptionFactory transformationFactory = new ChaosGameDescriptionFactory();
    public static final String JULIA_TRANSFORM = "Julia";
    public static final String SIERPINSKI_TRANSFORM = "Sierpinski";
    public static final String BARNSLEYFERN_TRANSFORM = "Barnsley-fern";
    public static final String AFFINE_TRANSFORM = "affine";
    private int steps;
    private static final Logger logger = LogManager.getLogger(ChaosGameHandler.class);
    private SaveState previousSaveState;

    /**
     * Constructor for the ChaosGameHandler class
     * It sets all the variables needed to create the chaos game.
     * It also adds the transformations and creates the canvas
     *
     * @since 0.0.1
     */
    public ChaosGameHandler() {
        transformationFactory = new ChaosGameDescriptionFactory();
    }

    /**
     * Runs the chaos game with the specified number of steps. This will update the 
     * underlying array which then has to be drawn to the screen.
     * @param steps the number of steps to run
     */
    public void run(int steps){
        chaosGame.runSteps(steps);
    }

    public void setSteps(int steps) {
        this.steps = steps;
    }

    public SaveState getPreviousSaveState() {
        return previousSaveState;
    }

    public boolean loadSaveState(boolean affine) throws IOException {
        SaveManager manager = SaveManager.getInstance();
        SaveState state = null;
        try {
            state = manager.loadSaveState();
            previousSaveState = state;
        } catch (IOException ioException) {
            logger.error("Failed to laod savestate.", ioException);
            throw ioException;
        }
        if (state == null) {
            return false; // There was no state saved
        }
        if (state.getTransformationType().equals(JULIA_TRANSFORM) && affine) {
            return false; // Incorrect transformation
        }
        setUpFromSave(state);
        return true;
    }

    private void setUpFromSave(SaveState state) {
        List<double[]> transformations;
        if (state.getTransformationType().equals(JULIA_TRANSFORM)) {
            transformations = SaveStateConverter.juliaTransformToArray(state.getJuliaTransforms());
            chaosGameDescription = transformationFactory.getCustomDescription(state.getMinCoords(), state.getMaxCoords(), transformations, false);
        } else {
            transformations = SaveStateConverter.affineTransformToArray(state.getAffineTransforms());
            chaosGameDescription = transformationFactory.getCustomDescription(state.getMinCoords(), state.getMaxCoords(), transformations, true);
        }
        chaosGameDescription.setTransformType(state.getTransformationType());
        chaosGame = new ChaosGame(chaosGameDescription);
        chaosGame.setTransformOrder(state.getTransformOrder());
        chaosGame.setReRunSteps(state.getReRunSteps());
        chaosGameCanvas = new ChaosGameCanvas(state.getWidth(), state.getHeight(), state.getMinCoords(), state.getMaxCoords());
        chaosGame.setChaosGameCanvas(chaosGameCanvas);
        chaosGameCanvas.setCanvas(state.getCanvas());
        steps = state.getSteps();
    }

    public void save(double windowWidth, double windowHeight) throws IOException {
        if (chaosGameCanvas == null) {
            return; // There is nothing to save
        }
        SaveState state = new SaveStateBuilder()
        .canvas(chaosGameCanvas.getCanvas())
        .height(chaosGameCanvas.getHeight())
        .width(chaosGameCanvas.getWidth())
        .steps(steps)
        .maxCoords(chaosGameCanvas.getMaxCoords())
        .minCoords(chaosGameCanvas.getMinCoords())
        .windowHeight(windowHeight)
        .windowWidth(windowWidth)
        .allPoints(getCanvas().getAllPoints())
        .transformOrder(chaosGame.getTransformOrder())
        .reRunSteps(chaosGame.getReRunSteps())
        .build();

        if (chaosGameDescription.getTransformType().equals(JULIA_TRANSFORM)) {
            ArrayList<JuliaTransform> juliaTransforms = new ArrayList<>();
            for (Transform2D juliaTransform : chaosGameDescription.getTransforms()) {
                juliaTransforms.add((JuliaTransform)juliaTransform);
            }
            state.setJuliaTransforms(juliaTransforms);
            state.setTransformType(chaosGameDescription.getTransformType());
        } else {
            ArrayList<AffineTransform2D> affineTransforms = new ArrayList<>();
            for (Transform2D affineTransform : chaosGameDescription.getTransforms()) {
                affineTransforms.add((AffineTransform2D)affineTransform);
            }
            state.setAffineTransforms(affineTransforms);
            state.setTransformType(chaosGameDescription.getTransformType());
        }

        SaveManager manager = SaveManager.getInstance();
        manager.saveSaveState(state);
    }

    /**
     * Initializes the chaos game with the specified number of steps.
     * @param steps the number of steps to run
     */
    public void init(int steps){
        run(steps);
    }

    public void reRun() {
        chaosGame.generatePreviousFractal();
    }

    /**
     * @return the canvas containing the underlying array for the chaos game.
     */
    public ChaosGameCanvas getCanvas() {
        return chaosGameCanvas;
    }

    /**
     * @return the chaos game description
     */
    public int getSteps() {
        return steps;
    }

    /**
     * Creates the chaos game with the current chaos game description.
     * It also creates the canvas for the chaos game.
     *
     * @since 0.0.4
     */
    public void createChaosGame(int width, int height, boolean reRun) {
        if (!reRun) {
            chaosGame = new ChaosGame(chaosGameDescription);
        }
        chaosGameCanvas = new ChaosGameCanvas(width, height, chaosGameDescription.getMinCoords(), chaosGameDescription.getMaxCoords());
        chaosGame.setChaosGameCanvas(chaosGameCanvas);
    }


    /**
     * Sets the chaos game description to the Julia fractal.
     * It is a preset fractal with a specific set of transformations.
     *
     * @since 0.0.4
     */
    public void setJuliaDescription(int width, int height){
        chaosGameDescription = transformationFactory.getDescriptionFromFile(JULIA_TRANSFORM);
        createChaosGame(width, height, false);
    }

    /**
     * Sets the chaos game description to the Sierpinski fractal.
     * It is a preset fractal with a specific set of transformations.
     *
     * @since 0.0.4
     */
    public void setSierpinskiDescription(int width, int height){
        chaosGameDescription = transformationFactory.getDescriptionFromFile(SIERPINSKI_TRANSFORM);
        createChaosGame(width, height, false);
    }

    /**
     * Sets the chaos game description to the Barnsley Fern fractal.
     * It is a preset fractal with a specific set of transformations.
     *
     * @since 0.0.4
     */
    public void setBarnsleyFernDescription(){
        chaosGameDescription = transformationFactory.getDescriptionFromFile(BARNSLEYFERN_TRANSFORM);
        createChaosGame(getCanvas().getWidth(), getCanvas().getHeight(), false);
    }

    public void setCustomDescription(Transformation fractal, boolean isAffine) {
        if (fractal == null) {
            throw new IllegalArgumentException("Fractal cannot be null");
        }
        if(isAffine) {
            chaosGameDescription = transformationFactory.getCustomDescriptionAffine(fractal.getMinCoords(), fractal.getMaxCoords(), fractal.getAffineTransform());
        } else {
            chaosGameDescription = transformationFactory.getCustomDescriptionJulia(fractal.getMinCoords(), fractal.getMaxCoords(), fractal.getJuliaTransforms());
        }
        createChaosGame(getCanvas().getWidth(), getCanvas().getHeight(), false);
    }

    /**
     *
     * @return the chaos game description
     * @since 0.0.4
     */
    public ChaosGameDescription getChaosGameDescription() {
        return chaosGameDescription;
    }

    /**
     * Sets the chaos game description
     * @param chaosGameDescription the chaos game description to set
     * @since 0.0.4
     */
    public void setChaosGameDescription(ChaosGameDescription chaosGameDescription) {
        this.chaosGameDescription = chaosGameDescription;
    }

}


