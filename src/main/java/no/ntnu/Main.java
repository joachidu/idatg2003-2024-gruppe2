package no.ntnu;

public class Main {

    public static void main(String[] args) {
        System.setProperty("log4j.configurationFile", "classpath:config/log4j2.xml");
        FxApplication application = new FxApplication();
        application.startFX(args);
    }
}